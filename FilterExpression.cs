﻿// ReSharper disable StaticMemberInGenericType

namespace Filter;

// ReSharper disable once UnusedType.Global
public class FilterExpression<T>
{
	private static readonly char[] DefaultInDelimiters = { ',' };

	private readonly List<(string Property, CONDITION Condition, string Value)> Ands = new(),
	                                                                            Ors  = new();

	// ReSharper disable once FieldCanBeMadeReadOnly.Global
	public char[] InDelimiters = DefaultInDelimiters;

	public FilterExpression()
	{
		var Type = typeof( T );

		// Class / Struct
		if( Type.IsClass || Type is { IsPrimitive: false, IsValueType: true } )
		{
			var Name = Type.FullName!;

			lock( PropertyCache )
			{
				if( !PropertyCache.TryGetValue( Name, out var Props ) )
				{
					PropertyCache.Add( Name, Props = ( from P in Type.GetProperties()
					                                   select P ).ToDictionary( info => info.Name, info => info ) );
				}
				Properties = Props;
			}
		}
		else
			throw new ArgumentException( $"FilterExpression cannot be of type: {Type.Name}" );
	}

	public FilterExpression( T objectOfType ) : this()
	{
		SetObject( objectOfType );
	}

	public bool Evaluate( IEnumerable<(bool JoinWithOr, string Property, string Condition, string Value)> expression )
	{
		Expression = expression;
		return Evaluate();
	}

	public IList<T> Evaluate( IEnumerable<T> objects,
	                          IEnumerable<(bool JoinWithOr, string Property, string Condition, string Value)> expression,
	                          IEnumerable<(string ApplicationPropertyName, bool Decending )> sorting )
	{
		SortOrder = sorting;
		return Evaluate( objects, expression );
	}

	public IList<T> Evaluate( IEnumerable<T> objects,
	                          IEnumerable<(bool JoinWithOr, string Property, string Condition, string Value)> expression )
	{
		Expression = expression;

		var Result = Evaluate( objects );

		if( Sorting )
			Result = Sort( Result );

		return Result;
	}

	public IList<T> Evaluate( IEnumerable<T> objects ) => ( from O in objects
	                                                        where Evaluate( O )
	                                                        select O ).ToList();

	public bool Evaluate( T obj )
	{
		Object = obj;
		return Evaluate();
	}

	public bool Evaluate()
	{
		bool Result = true,
		     DoingOrs;

		if( _Expression.Any() )
		{
			bool DoValue( (string Property, CONDITION Condition, string Value) value )
			{
				var (Property, Condition, Val) = value;

				if( Val.Length == 0 )
					throw new ArgumentException( $"Missing value for property: {Property}" );

				try
				{
					var ObjectValue = PropertyValue( Property );

					void Doing( bool val )
					{
						if( DoingOrs )
							Result |= val;
						else
							Result &= val;
					}

					void DoCondition( decimal cmp )
					{
						Doing( Condition switch
						       {
							       CONDITION.LESS_THAN_EQUAL_TO when cmp <= 0    => true,
							       CONDITION.GREATER_THAN_EQUAL_TO when cmp >= 0 => true,
							       CONDITION.EQUAL_TO when cmp == 0              => true,
							       CONDITION.NOT_EQUAL_TO when cmp != 0          => true,
							       CONDITION.LESS_THAN when cmp < 0              => true,
							       CONDITION.GREATER_THAN when cmp > 0           => true,
							       _                                             => false
						       } );
					}

					string? Value;

					// Reference to another property
					if( Val[ 0 ] == '.' )
					{
					#if NET5_0_OR_GREATER
						var Prop = Val[ 1.. ];
					#else
						// ReSharper disable once ReplaceSubstringWithRangeIndexer
						var Prop = Val.Substring( 1 );
					#endif
						var PropVal = PropertyValue( Prop );
						Value = PropVal.ToString();
					}
					else
						Value = Val;

					if( Value is null )
						// ReSharper disable once NotResolvedInText
						throw new ArgumentNullException( "Null Value." );

					// Special String Operators
					switch( Condition )
					{
					case CONDITION.STARTS_WITH:
					case CONDITION.ENDS_WITH:
					case CONDITION.CONTAINS:
					case CONDITION.IN:
						var LVal = ObjectValue is not string SVal ? ObjectValue.ToString() : SVal;

						if( LVal is null )
							throw new Exception(); // Caught later

						// ReSharper disable once SwitchExpressionHandlesSomeKnownEnumValuesWithExceptionInDefault
						Doing( Condition switch
						       {
							       CONDITION.STARTS_WITH => LVal.StartsWith( Value, StringComparison.OrdinalIgnoreCase ),
							       CONDITION.ENDS_WITH   => LVal.EndsWith( Value, StringComparison.OrdinalIgnoreCase ),
							       CONDITION.CONTAINS    => LVal.ToUpper().Contains( Value.ToUpper() ),
							       CONDITION.IN          => LVal.Split( InDelimiters, StringSplitOptions.RemoveEmptyEntries ).Any( s => string.Compare( s, Value, StringComparison.OrdinalIgnoreCase ) == 0 ),
							       _                     => throw new ArgumentOutOfRangeException( nameof( Expression ) )
						       } );
						break;

					default:
						switch( ObjectValue )
						{
						case bool B:
							DoCondition( B == bool.Parse( Value ) ? 0 : -1 );
							break;

						case string Str:
							DoCondition( string.CompareOrdinal( Str, Value ) );
							break;

						case byte B:
							DoCondition( B - short.Parse( Value ) );
							break;

						case sbyte S:
							DoCondition( S - sbyte.Parse( Value ) );
							break;

						case short S:
							DoCondition( S - short.Parse( Value ) );
							break;

						case ushort U:
							DoCondition( U - int.Parse( Value ) );
							break;

						case int I:
							DoCondition( I - int.Parse( Value ) );
							break;

						case uint U:
							DoCondition( U - long.Parse( Value ) );
							break;

						case long L:
							DoCondition( L - long.Parse( Value ) );
							break;

						case ulong L:
							long C;
							var  Temp = ulong.Parse( Value );

							if( L < Temp )
								C = -1;
							else if( L > Temp )
								C = 1;
							else
								C = 0;

							DoCondition( C );
							break;

						case float F:
							DoCondition( (decimal)( F - float.Parse( Value ) ) );
							break;

						case double D:
							DoCondition( (decimal)( D - double.Parse( Value ) ) );
							break;

						case decimal D:
							DoCondition( D - decimal.Parse( Value ) );
							break;

						case DateTime D:
							DoCondition( D.Ticks - DateTime.Parse( Value ).Ticks );
							break;

						case DateTimeOffset D:
							DoCondition( D.Ticks - DateTimeOffset.Parse( Value ).Ticks );
							break;

						default:
							var E = IsEnum( ObjectValue );

							if( E.IsEnum )
							{
								foreach( var EValue in E.Values )
								{
									if( EValue.Key == Value )
									{
										DoCondition( E.Value - EValue.Value );
										break;
									}
								}
								throw new ArgumentException( $"Unknown Enum Value: {Value}" );
							}
							throw new UnsupportedException( $"Unsupported type: {ObjectValue.GetType().Name}" );
						}
						break;
					}
				}
				catch( Exception Exception ) when( Exception is not UnsupportedException )
				{
					throw new ArgumentException( $"Cannot convert property: {Property}" );
				}
				return Result;
			}

			// Do Or Conditions First
			if( Ors.Count > 0 )
			{
				DoingOrs = true;
				Result   = false;

				foreach( var Or in Ors )
				{
					if( DoValue( Or ) )
						break;
				}
			}

			DoingOrs = false;

			foreach( var And in Ands )
			{
				if( !DoValue( And ) )
					break;
			}
		}
		return Result;
	}

	private class UnsupportedException : ArgumentException
	{
		public UnsupportedException( string message ) : base( message )
		{
		}
	}

#region Enum
	private static readonly Dictionary<string, Dictionary<string, int>> EnumCache = new();

	private static (bool IsEnum, int Value, Dictionary<string, int> Values) IsEnum( object obj )
	{
		var Type = obj.GetType();

		if( Type.IsEnum )
		{
			var FullName = Type.FullName!;

			lock( EnumCache )
			{
				if( !EnumCache.TryGetValue( FullName, out var Dict ) )
				{
					var Names = Enum.GetNames( Type );

					Dict = new Dictionary<string, int>();

					foreach( var Name in Names )
					{
						var Value = (int)Enum.Parse( Type, Name );
						Dict.Add( Name, Value );
					}

					EnumCache.Add( FullName, Dict );
				}
				return ( true, (int)obj, Dict );
			}
		}
		return ( false, 0, null! );
	}
#endregion

#region Sorting
	private IEnumerable<(string ApplicationPropertyName, bool Decending )> _SortOrder = Enumerable.Empty<(string ApplicationPropertyName, bool Decending )>();
	private bool                                                           Sorting;

	public IEnumerable<(string ApplicationPropertyName, bool Decending )> SortOrder
	{
		get => _SortOrder;
		set
		{
			_SortOrder = value;
			Sorting    = value.Any();
		}
	}

	private List<( T OriginalClassObject, List<(object Value, bool Decending)>)> ToSortList( IEnumerable<T> objects ) => ( from Obj in objects
	                                                                                                                       select ( Obj,
	                                                                                                                                ( from Order in SortOrder
	                                                                                                                                  select ( PropertyValue( Order.ApplicationPropertyName ), Order.Decending ) ).ToList()
	                                                                                                                              ) ).ToList();

	public IList<T> Sort( IEnumerable<T> objects )
	{
		var SortList = ToSortList( objects );
		SortList.Sort( Comparison );

		return ( from S in SortList
		         select S.OriginalClassObject ).ToList();
	}


	private static int Comparison( (T OriginalClassObject, List<(object Value, bool Decending)> Members) x, (T OriginalClassObject, List<(object Value, bool Decending)> Members) y )
	{
		var Ndx = 0;
		var Cmp = 0;

		foreach( var X in x.Members )
		{
			var YValue = y.Members[ Ndx++ ].Value;

			switch( X.Value )
			{
			case bool B:
				Cmp = B == (bool)YValue ? 0 : -1;
				break;

			case string Str:
				Cmp = string.CompareOrdinal( Str, (string)YValue );
				break;

			case byte B:
				{
					var UyVal = (short)YValue;

					if( B < UyVal )
						Cmp = -1;
					else if( B > UyVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case sbyte S:
				Cmp = S - (sbyte)YValue;
				break;

			case short S:
				Cmp = S - (short)YValue;
				break;

			case ushort U:
				{
					var UyVal = (ushort)YValue;

					if( U < UyVal )
						Cmp = -1;
					else if( U > UyVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case int I:
				Cmp = I - (int)YValue;
				break;

			case uint U:
				{
					var UyVal = (uint)YValue;

					if( U < UyVal )
						Cmp = -1;
					else if( U > UyVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case long L:
				{
					var YVal = (long)YValue;

					if( L < YVal )
						Cmp = -1;
					else if( L > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case ulong U:
				{
					var UyVal = (ulong)YValue;

					if( U < UyVal )
						Cmp = -1;
					else if( U > UyVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case float F:
				{
					var YVal = (float)YValue;

					if( F < YVal )
						Cmp = -1;
					else if( F > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case double D:
				{
					var YVal = (double)YValue;

					if( D < YVal )
						Cmp = -1;
					else if( D > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case decimal D:
				{
					var YVal = (decimal)YValue;

					if( D < YVal )
						Cmp = -1;
					else if( D > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case DateTime D:
				{
					var XVal = D.Ticks;
					var YVal = ( (DateTime)YValue ).Ticks;

					if( XVal < YVal )
						Cmp = -1;
					else if( XVal > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			case DateTimeOffset D:
				{
					var XVal = D.Ticks;
					var YVal = ( (DateTimeOffset)YValue ).Ticks;

					if( XVal < YVal )
						Cmp = -1;
					else if( XVal > YVal )
						Cmp = 1;
					else
						Cmp = 0;
				}
				break;

			default:
				var E = IsEnum( X.Value );

				if( E.IsEnum )
				{
					var E2 = IsEnum( YValue );

					if( E2.IsEnum )
					{
						Cmp = E.Value - E2.Value;
						break;
					}
				}
				throw new UnsupportedException( $"Unsupported type: {X.Value.GetType().Name}" );
			}

			if( Cmp != 0 )
			{
				if( X.Decending )
				{
					if( Cmp < 0 )
						Cmp = 1;
					else
						Cmp = -1;
				}
				break;
			}
		}
		return Cmp;
	}
#endregion

#region Properties
	private static readonly Dictionary<string, Dictionary<string, PropertyInfo>> PropertyCache = new();
	private readonly        Dictionary<string, PropertyInfo>                     Properties;

	private PropertyInfo PropertyInfo( string property )
	{
		// Translate Application Property
		if( PMap.TryGetValue( property, out var Prop ) )
			property = Prop;

		if( !Properties.TryGetValue( property, out var Info ) )
			throw new ArgumentException( $"Not be a property: {property}" );

		return Info;
	}

	private object PropertyValue( string property )
	{
		var ObjectValue = PropertyInfo( property ).GetValue( ObjectOfType );

		if( ObjectValue is null )
			throw new ArgumentNullException( $"Null Property: {property}" );

		return ObjectValue;
	}
#endregion

#region Property Map
	private (string ApplicationProperty, string ObjectProperty)[] _PropertyMap = Array.Empty<(string ApplicationProperty, string ObjectProperty)>();

	private Dictionary<string, string> PMap = new();

	public (string ApplicationProperty, string ObjectProperty)[] PropertyMap
	{
		get => _PropertyMap;

		set
		{
			_PropertyMap = value;

			PMap = ( from V in value
			         select V ).ToDictionary( entry => entry.ApplicationProperty, entry => entry.ObjectProperty );
		}
	}
#endregion

#region CONDITION
	private enum CONDITION
	{
		LESS_THAN,
		EQUAL_TO,
		NOT_EQUAL_TO,
		GREATER_THAN,
		LESS_THAN_EQUAL_TO,
		GREATER_THAN_EQUAL_TO,

		// Special String Conditions
		IN,
		CONTAINS,
		STARTS_WITH,
		ENDS_WITH
	}

	private static CONDITION ToCondition( string condition )
	{
		return condition.Trim() switch
		       {
			       "<"  => CONDITION.LESS_THAN,
			       ">"  => CONDITION.GREATER_THAN,
			       "==" => CONDITION.EQUAL_TO,
			       "!=" => CONDITION.NOT_EQUAL_TO,
			       ">=" => CONDITION.GREATER_THAN_EQUAL_TO,
			       "<=" => CONDITION.LESS_THAN_EQUAL_TO,
			       _ => condition.ToUpper() switch
			            {
				            "S"           => CONDITION.STARTS_WITH,
				            "STARTS WITH" => CONDITION.STARTS_WITH,

				            "E"         => CONDITION.ENDS_WITH,
				            "ENDS WITH" => CONDITION.ENDS_WITH,

				            "C"        => CONDITION.CONTAINS,
				            "CONTAINS" => CONDITION.CONTAINS,

				            "I"  => CONDITION.IN,
				            "IN" => CONDITION.IN,

				            _ => throw new ArgumentOutOfRangeException( condition )
			            }
		       };
	}
#endregion

#region Object
	private T ObjectOfType = default!;

	public T Object
	{
		// ReSharper disable once UnusedMember.Global
		get => ObjectOfType;
		set => SetObject( value );
	}

	private void SetObject( T obj )
	{
		ObjectOfType = obj;

		if( obj is null )
			// ReSharper disable once NotResolvedInText
			throw new ArgumentNullException( "objectOfType" );
	}
#endregion

#region Expression
	private IEnumerable<(bool JoinWithOr, string Property, string Condition, string Value)> _Expression = Enumerable.Empty<(bool JoinWithOr, string Property, string Condition, string Value)>();

	public IEnumerable<(bool JoinWithOr, string Property, string Condition, string Value)> Expression
	{
		get => _Expression;
		set
		{
			_Expression = value;

			// Split into And an Or
			Ands.Clear();
			Ors.Clear();

			if( value.Any() )
			{
				var First = true;

				foreach( var Exp in value )
				{
					(string Property, CONDITION Condition, string Value) E;

					E.Property  = Exp.Property.Trim();
					E.Condition = ToCondition( Exp.Condition );
					E.Value     = Exp.Value.Trim();

					if( First || Exp.JoinWithOr )
					{
						First = false;
						Ors.Add( E );
					}
					else
						Ands.Add( E );
				}
			}
		}
	}
#endregion
}