﻿// ReSharper disable ReplaceSliceWithRangeIndexer

namespace Utils;

public static class Strings
{
	public static bool Contains( this string txt, string[] strings ) => strings.Any( txt.Contains );
	public static bool Contains( this string txt, char[] strings ) => strings.Any( txt.Contains );
	public static bool Contains( this string text, string value, StringComparison stringComparison ) => text.IndexOf( value, stringComparison ) >= 0;

	public static bool ContainsAll( this IEnumerable<string> container, IEnumerable<string> values ) => container.All( values.Contains );

	public static bool Contains( this IEnumerable<string> text, string value, StringComparison stringComparison )
	{
		return text.Any( t => string.Compare( t, value, stringComparison ) == 0 );
	}

	public static string ToHex( this string s, string separator = " ", bool zeroSuppress = true )
	{
		var Builder = new StringBuilder();

		static char ToChar( byte nibble )
		{
			nibble &= 0xf;

			if( nibble <= 9 )
				return (char)( (byte)'0' + nibble );

			return (char)( ( (byte)'A' + nibble ) - 10 );
		}

		static string ByteToStr( byte b )
		{
			return ToChar( (byte)( b >> 4 ) ).ToString() + ToChar( b );
		}

		static string ToStr( ushort word )
		{
			return ByteToStr( (byte)( word >> 8 ) ) + ByteToStr( (byte)word );
		}

		var First = true;

		foreach( var C in s )
		{
			if( !First )
				Builder.Append( separator );
			else
				First = false;

			var Hex = ToStr( C );

			if( zeroSuppress )
			{
				Hex = Hex.TrimStart( '0' );

				switch( Hex.Length )
				{
				case 0:
					Hex = "00";
					break;

				case 1:
				case 3:
					Hex = '0' + Hex;
					break;
				}
			}

			Builder.Append( Hex );
		}

		return Builder.ToString();
	}

	public static string ToUnicodeString( this byte[] bytes )
	{
		var BSpan = new ReadOnlySpan<byte>( bytes );

		var Length = BSpan.Length;

		return Length switch
		       {
			       0 => "",
			       1 => ( (char)BSpan[ 0 ] ).ToString(),
			       _ => BSpan[ 0 ] switch
			            {
				            // .Net 4.7.2 no Range Indexer
				            0xef when ( Length > 3 ) && ( BSpan[ 1 ] == 0xbb ) && ( BSpan[ 2 ] == 0xbf )                     => Encoding.UTF8.GetString( BSpan.Slice( 3 ).ToArray() ),
				            0xfe when ( Length > 2 ) && ( BSpan[ 1 ] == 0xff )                                               => Encoding.BigEndianUnicode.GetString( BSpan.Slice( 2 ).ToArray() ),
				            0xff when ( Length > 2 ) && ( BSpan[ 1 ] == 0xfe )                                               => Encoding.Unicode.GetString( BSpan.Slice( 2 ).ToArray() ),
				            0 when ( Length > 4 ) && ( BSpan[ 1 ] == 0 ) && ( BSpan[ 2 ] == 0xfe ) && ( BSpan[ 3 ] == 0xff ) => throw new NotSupportedException( "UTF-32 Big Endian Not Supported" ),
				            0xff when ( Length > 4 ) && ( BSpan[ 1 ] == 0xfe ) && ( BSpan[ 2 ] == 0 ) && ( BSpan[ 3 ] == 0 ) => Encoding.UTF32.GetString( BSpan.Slice( 4 ).ToArray() ),
				            _                                                                                                => Encoding.UTF8.GetString( bytes )
			            }
		       };
	}

	public static string AlphaNumericAndSpaces( this string str, bool trim = false, bool pack = false, bool allowTabs = true )
	{
		var Result = new StringBuilder();

		foreach( var C in str )
		{
			switch( C )
			{
			case (char)0x8:
				if( allowTabs )
					Result.Append( ' ' );
				break;

			case ' ':
				Result.Append( ' ' );
				break;

			case (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or (>= '0' and <= '9'):
				Result.Append( C );
				break;
			}
		}

		var Str = Result.ToString();

		return pack ? Str.Pack( trim ) :
		       trim ? Str.Trim() : Str;
	}

	public static string ReplaceLastOccurrence( this string source, string find, string replace, StringComparison comparison = StringComparison.Ordinal )
	{
		var Place = source.LastIndexOf( find, comparison );

		return Place != -1 ? source.Remove( Place, find.Length ).Insert( Place, replace ) : source;
	}

	public static string TrimStart( this string target, string trimString, StringComparison comparison )
	{
		if( !string.IsNullOrEmpty( target ) && !string.IsNullOrEmpty( trimString ) )
		{
			var Length = trimString.Length;

			while( target.StartsWith( trimString, comparison ) )
				// ReSharper disable once ReplaceSubstringWithRangeIndexer
				target = target.Substring( Length );
		}
		return target;
	}

	public static string TrimToLower( this string txt ) => txt.Trim().ToLower();
	public static string TrimToUpper( this string txt ) => txt.Trim().ToUpper();

	public static string NullTrim( this string? str ) => str is null ? "" : str.Trim();

	public static string SubStr( this string str, int startIndex, int length )
	{
		try
		{
			return ( startIndex + length ) > str.Length ? str.Substring( startIndex ) : str.Substring( startIndex, length );
		}
		catch
		{
			return "";
		}
	}

	public static string ToNumeric( this string txt, bool allowDecimal = false )
	{
		var HasDecimal = !allowDecimal;

		if( HasDecimal ) // Remove anything from the decimal point onwards
		{
			var Pos = txt.IndexOf( '.' );

			if( Pos >= 0 )
				// ReSharper disable once ReplaceSubstringWithRangeIndexer
				txt = txt.Substring( 0, Pos );
		}

		var HasSign  = false;
		var HasDigit = false;

		var Result = new StringBuilder();

		foreach( var C in txt )
		{
			switch( C )
			{
			case '+':
			case '-':
				if( !HasSign )
				{
					if( C != '+' )
						Result.Append( C );
				}

				break;

			case '.':
				if( !HasDecimal )
				{
					HasDecimal = true;
					Result.Append( HasDigit ? "." : "0." );
				}

				break;

			default:
				if( C is >= '0' and <= '9' )
				{
					HasDigit = true;
					Result.Append( C );

					break;
				}
				continue;
			}

			HasSign = true;
		}

		return Result.ToString().TrimEnd( '.', '-' );
	}

	public static bool ToBool( this string value )
	{
		return value.TrimToLower() switch
		       {
			       "true" => true,
			       "t"    => true,
			       _      => false
		       };
	}

	public static int Compare( this string str1, string str2, StringComparison strCmp = StringComparison.InvariantCulture ) => string.Compare( str1, str2, strCmp );
	public static bool CompareEqualsIgnoreCase( this string str1, string str2, StringComparison strCmp = StringComparison.InvariantCulture ) => str1.Compare( str2, StringComparison.OrdinalIgnoreCase ) == 0;

	public static bool IsNullOrEmpty( this string? str ) => string.IsNullOrEmpty( str );

	public static bool IsNotNullOrEmpty( this string? str ) => !string.IsNullOrEmpty( str );

	public static bool IsNullOrWhiteSpace( this string? str ) => string.IsNullOrWhiteSpace( str );

	public static bool IsNotNullOrWhiteSpace( this string? str ) => !string.IsNullOrWhiteSpace( str );

	public static bool IsAlpha( this char c ) => c is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z');

	public static bool IsAlphaNumeric( this char c ) => c is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or (>= '0' and <= '9');

	public static bool IsAlphaNumeric( this string txt )
	{
		foreach( var C in txt )
		{
			if( !C.IsAlphaNumeric() )
				return false;
		}
		return true;
	}

	public static string Capitalise( this string value ) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase( value.ToLower() );

	public static string TrimToCapitalise( this string txt ) => txt.Trim().Capitalise();

	public static string ToAlphaNumeric( this string txt )
	{
		var Result = new StringBuilder();

		if( txt.IsNotNullOrWhiteSpace() )
		{
			foreach( var C in txt )
			{
				if( C.IsAlphaNumeric() )
					Result.Append( C );
			}
		}

		return Result.ToString();
	}

	public static string ToIdent( this string value )
	{
		var First  = true;
		var Retval = new StringBuilder();

		foreach( var C in value )
		{
			if( C is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or '_' || ( !First && C is >= '0' and <= '9' ) )
				Retval.Append( C );
			else
				Retval.Append( '_' );

			First = false;
		}

		return Retval.ToString();
	}

	public static bool IsInteger( this string value ) => long.TryParse( value, out _ );
	public static bool IsNumeric( this string value ) => double.TryParse( value, out _ );

	public static string? Between( this string text, string first, string last, StringComparison culture = StringComparison.Ordinal )
	{
		var First = text.IndexOf( first, culture );

		if( First >= 0 )
		{
			First += first.Length;

			var Last = text.LastIndexOf( last, culture );

			if( ( Last >= 0 ) && ( Last > First ) )
				return text.Substring( First, Last - First );
		}

		return null;
	}

	public static string Pack( this string str, bool trim = true )
	{
		if( trim )
			str = str.Trim();

		var Result = new StringBuilder();

		var WasSpace = false;

		foreach( var C in str )
		{
			if( C <= ' ' )
			{
				if( !WasSpace )
				{
					WasSpace = true;
					Result.Append( ' ' );
				}
			}
			else
			{
				WasSpace = false;
				Result.Append( C );
			}
		}
		return Result.ToString();
	}

	public static bool IsIdent( this string id )
	{
		var First = true;

		foreach( var C in id )
		{
			if( C is (>= 'a') and (<= 'z') or (>= 'A' and <= 'Z') || ( !First && C is (>= '0' and <= '9') ) )
				First = false;
			else
				return false;
		}

		return true;
	}

	public static bool ContainsNumber( this string id )
	{
		foreach( var C in id )
		{
			if( C is (>= '0' and <= '9') )
				return true;
		}
		return false;
	}

	public static (string Suite, string StreetNumber, string Street) SplitAddress( this string addressLine )
	{
	#if NET7_0_OR_GREATER
		var SplitAddress = addressLine.Split( ' ', StringSplitOptions.RemoveEmptyEntries );
	#else
		var SplitAddress = addressLine.Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );
	#endif
		var Num0 = SplitAddress[ 0 ].ContainsNumber();
		var Num1 = SplitAddress[ 1 ].ContainsNumber();

		string Suite, StreetNumber, Street;
		int    Skip;

		switch( Num0 )
		{
		case true when Num1:
			Suite        = SplitAddress[ 0 ];
			StreetNumber = SplitAddress[ 1 ];
			Street       = SplitAddress[ 2 ];
			Skip         = 3;
			break;

		case true:
			Suite        = "";
			StreetNumber = SplitAddress[ 0 ];
			Street       = SplitAddress[ 1 ];
			Skip         = 2;
			break;

		default:
			Suite        = "";
			StreetNumber = "";
			Street       = SplitAddress[ 0 ];
			Skip         = 1;
			break;
		}
		return ( Suite, StreetNumber, $"{Street} {string.Join( " ", SplitAddress.Skip( Skip ) )}" );
	}


	public static string MaxLength( this string? value, int maxLength ) => value is null ? "" : value.Substring( 0, Math.Min( value.Length, maxLength ) );

	public static IList<string> Chunks( this string? value, int chunkSize, int maxChunks )
	{
		var Result = new List<string>();

		if( ( maxChunks > 0 ) && value is not null )
		{
			var Remaining = value.Length;

			if( Remaining > 0 )
			{
				var Chunks = Math.Min( ( Remaining / chunkSize ) + 1, maxChunks );
				var Span   = value.AsSpan();

				for( var Ndx = 0; ( Remaining > 0 ) && ( Ndx < Chunks ); ++Ndx, Remaining -= chunkSize )
				{
					var Size = Math.Min( Remaining, chunkSize );
					Result.Add( Span.Slice( Ndx * chunkSize, Size ).ToString() );
				}
			}
		}
		return Result;
	}
}