﻿namespace Utils;

public static class DateTimeExtensions
{
	public static DateTimeOffset DateTimeATimeOffset( this int offset ) => DateTime.UtcNow.DateTimeATimeOffset( offset );

	private static DateTimeOffset DateTimeATimeOffset( this DateTime time, int offset )
	{
		// Get the current UTC DateTime
		var UtcDateTime = time.ToUniversalTime();

		// Create a DateTime with DateTimeKind.Unspecified
		var UnspecifiedNow = DateTime.SpecifyKind( UtcDateTime, DateTimeKind.Unspecified );

		// Create a TimeSpan for the desired offset
		var OffsetTimeSpan = TimeSpan.FromHours( offset );

		// Add the offset to the unspecified DateTime and return a new DateTimeOffset with the desired offset
		return new DateTimeOffset( UnspecifiedNow.AddHours( offset ), OffsetTimeSpan );
	}

	public static DateTimeOffset DateTimeATimeOffset( this DateTimeOffset time, int offset ) => time.DateTime.DateTimeATimeOffset( offset );

	public static (DayOfWeek DayOfWeek, int Ordinal) GetDayOfWeekAndOrdinal( this DateTimeOffset date )
	{
		var FirstDayOfMonth = new DateTimeOffset( date.Year, date.Month, 1, 0, 0, 0, date.Offset );
		var DayOfWeek       = date.DayOfWeek;

		var Ordinal = FirstDayOfMonth.DayOfWeek <= DayOfWeek
			              ? ( ( date.Day - 1 ) / 7 ) + 1
			              : ( date.Day + 6 ) / 7;

		return ( DayOfWeek, Ordinal );
	}

	/// <summary>
	///     Allows for 5th Monday, Tuesday, Wednesday of the month
	/// </summary>
	public static int WeekOfMonth( this DateTimeOffset dateTime )
	{
		var First = new DateTimeOffset( dateTime.Year, dateTime.Month, 1, 0, 0, 0, dateTime.Offset );

		var DayOfWeek = (int)First.DayOfWeek - 1;

		if( DayOfWeek < 0 ) // Make Monday the first day of the week
			DayOfWeek = 6;

		return ( (int)( dateTime - First ).TotalDays + DayOfWeek ) / 7;
	}

	public static int WeekOfMonth( this DateTime date ) => new DateTimeOffset( date ).WeekOfMonth();

#if XAMARIN
	public const string PACIFIC_STANDARD_TIME = "PST";
#else
	public const string PACIFIC_STANDARD_TIME = "Pacific Standard Time";
#endif

	public const string MONDAY_SHORT    = "MON",
	                    TUESDAY_SHORT   = "TUE",
	                    WEDNESDAY_SHORT = "WED",
	                    THURSDAY_SHORT  = "THU",
	                    FRIDAY_SHORT    = "FRI",
	                    SATURDAY_SHORT  = "SAT",
	                    SUNDAY_SHORT    = "SUN";

	public enum WEEK_DAY : byte
	{
		UNKNOWN,
		MONDAY,
		TUESDAY,
		WEDNESDAY,
		THURSDAY,
		FRIDAY,
		SATURDAY,
		SUNDAY
	}

	// ReSharper disable once InconsistentNaming
	public static readonly TimeSpan END_OF_DAY   = new( 0, 23, 59, 59, 999 );
	public static readonly TimeSpan START_OF_DAY = TimeSpan.Zero;

	public static readonly DateTime Epoch;
	public static readonly TimeSpan ONE_HOUR = new( 1, 0, 0 );

	public static DateTimeOffset NowKindUnspecified    => DateTimeOffset.Now.KindUnspecified();
	public static DateTimeOffset UtcNowKindUnspecified => DateTimeOffset.UtcNow.KindUnspecified();

	private static TimeZoneInfo? _PacificTimeZoneInfo;

	public static TimeZoneInfo PacificTimeZoneInfo
	{
		get
		{
			if( _PacificTimeZoneInfo is null )
			{
			#if XAMARIN
				var Zones = TimeZoneInfo.GetSystemTimeZones();

				foreach( var Zone in Zones )
				{
					if( Zone.StandardName.Compare( PACIFIC_STANDARD_TIME, StringComparison.OrdinalIgnoreCase ) == 0 )
					{
						_PacificTimeZoneInfo = Zone;
						return Zone;
					}
				}
			#else
				return _PacificTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById( PACIFIC_STANDARD_TIME );
			#endif
			}
			return _PacificTimeZoneInfo;
		}
	}

	static DateTimeExtensions()
	{
		Epoch = new DateTime( 1970, 1, 1 );
	}

	public static string ShowAsLocalTime( this DateTimeOffset t, bool asLocal )
	{
		if( asLocal )
		{
			t = t.ToLocalTime();

			return $"{t:d} {t:h:mm:ss tt}";
		}

		return $"{t:d} {t:h:mm:ss tt zz}";
	}

	public static WEEK_DAY ToWeekDay( DateTime time )
	{
		return time.DayOfWeek switch
		       {
			       DayOfWeek.Monday    => WEEK_DAY.MONDAY,
			       DayOfWeek.Tuesday   => WEEK_DAY.TUESDAY,
			       DayOfWeek.Wednesday => WEEK_DAY.WEDNESDAY,
			       DayOfWeek.Thursday  => WEEK_DAY.THURSDAY,
			       DayOfWeek.Friday    => WEEK_DAY.FRIDAY,
			       DayOfWeek.Saturday  => WEEK_DAY.SATURDAY,
			       DayOfWeek.Sunday    => WEEK_DAY.SUNDAY,
			       _                   => WEEK_DAY.UNKNOWN
		       };
	}

	public static WEEK_DAY ToWeekDayFromShortText( string weekDay )
	{
		return weekDay.Trim().ToUpper() switch
		       {
			       MONDAY_SHORT    => WEEK_DAY.MONDAY,
			       TUESDAY_SHORT   => WEEK_DAY.TUESDAY,
			       WEDNESDAY_SHORT => WEEK_DAY.WEDNESDAY,
			       THURSDAY_SHORT  => WEEK_DAY.THURSDAY,
			       FRIDAY_SHORT    => WEEK_DAY.FRIDAY,
			       SATURDAY_SHORT  => WEEK_DAY.SATURDAY,
			       SUNDAY_SHORT    => WEEK_DAY.SUNDAY,
			       _               => WEEK_DAY.UNKNOWN
		       };
	}

	public static DateTimeOffset ToEndOfDay( this DateTimeOffset date ) => date.Date.Add( END_OF_DAY );

	public static DateTime ToEndOfDay( this DateTime date ) => date.Date.Add( END_OF_DAY );

	public static DateTimeOffset ServerTimeToLocalTime( this DateTime serverTime )
	{
		DateTimeOffset RetVal;

		if( serverTime.Date == DateTime.MinValue )
			RetVal = DateTimeOffset.MinValue;
		else
			RetVal = serverTime.Date < Epoch.Date ? serverTime : serverTime.ToLocalTime();

		var Temp = DateTime.SpecifyKind( RetVal.DateTime, DateTimeKind.Unspecified );

		RetVal = new DateTimeOffset( Temp, new TimeSpan( 0 ) );

		return RetVal;
	}

	public static DateTime LocalTimeToServerTime( this DateTimeOffset serverTime )
	{
		var Temp = DateTime.SpecifyKind( serverTime.DateTime, DateTimeKind.Local );

		return Temp < Epoch.Date ? serverTime.DateTime : Temp.ToLocalTime();
	}

	public static DateTimeOffset KindUnspecified( this DateTimeOffset time ) => new( DateTime.SpecifyKind( time.DateTime, DateTimeKind.Unspecified ), new TimeSpan( 0 ) );

	public static string ToTzDateTime( this DateTimeOffset date ) => date.ToString( "dd/MM/yyyy hh:mm tt zz" );

	public static long UnixTicksSeconds( this DateTime time ) => (long)( time - Epoch ).TotalSeconds;

	public static long UnixTicksSeconds( this DateTimeOffset time ) => (long)( time - Epoch ).TotalSeconds;

	public static long UnixTicksMilliSeconds( this DateTime time ) => (long)( time - Epoch ).TotalMilliseconds;
	public static long UnixTicksMilliSeconds() => DateTime.UtcNow.UnixTicksMilliSeconds();
	public static long UnixTicksMilliSeconds( this DateTimeOffset time ) => (long)( time - Epoch ).TotalMilliseconds;

	public static DateTime UnixTicksToDateTime( this long ticks ) => Epoch.AddMilliseconds( ticks );
	public static DateTime NoSeconds( this DateTime time ) => time.AddSeconds( -time.Second ).AddMilliseconds( -time.Millisecond );

	public static DateTimeOffset AsPacificStandardTime( this DateTime time )
	{
		if( time < Epoch )
			return Epoch;

		var UtcTime     = DateTime.SpecifyKind( time.ToUniversalTime(), DateTimeKind.Unspecified ); //.AddHours( EasternTimeZoneInfo.BaseUtcOffset.Hours );
		var Offset      = PacificTimeZoneInfo.BaseUtcOffset;
		var PacificTime = new DateTimeOffset( UtcTime, Offset );

		if( PacificTimeZoneInfo.IsDaylightSavingTime( PacificTime ) )
		{
			Offset      = Offset.Add( ONE_HOUR );
			PacificTime = new DateTimeOffset( UtcTime, Offset );
		}

		return PacificTime.FixDateTime();
	}

	public static DateTimeOffset AsPacificStandardTime( this DateTimeOffset time )
	{
		if( time < Epoch )
			return Epoch;

		var UtcTime     = DateTime.SpecifyKind( time.UtcDateTime, DateTimeKind.Unspecified ).AddHours( PacificTimeZoneInfo.BaseUtcOffset.Hours );
		var PacificTime = new DateTimeOffset( UtcTime, PacificTimeZoneInfo.BaseUtcOffset );

		if( PacificTimeZoneInfo.IsDaylightSavingTime( PacificTime ) )
			PacificTime = PacificTime.AddHours( 1 );

		return PacificTime.FixDateTime();
	}

	public static DateTimeOffset FromPacificStandardTime( this DateTime time )
	{
		if( time < Epoch )
			return Epoch;

		var PacificTime = new DateTimeOffset( time, PacificTimeZoneInfo.BaseUtcOffset );

		if( PacificTimeZoneInfo.IsDaylightSavingTime( PacificTime ) )
			PacificTime = PacificTime.AddHours( -1 );

		var LocalTime = PacificTime.ToLocalTime();

		return LocalTime.FixDateTime();
	}

	public static DateTimeOffset AsPacificStandardTime( this DateTime? deviceTime ) => deviceTime?.AsPacificStandardTime() ?? DateTimeOffset.MinValue;

	public static DateTimeOffset AsPacificStandardTime( this DateTimeOffset? deviceTime ) => deviceTime?.AsPacificStandardTime() ?? DateTimeOffset.MinValue;

	public static DateTimeOffset StartOfDay( this DateTimeOffset date ) => date.Date;
	public static DateTime StartOfDay( this DateTime date ) => date.Date;

	public static DateTimeOffset EndOfDay( this DateTimeOffset date ) => new DateTime( date.Year, date.Month, date.Day, 23, 59, 59, 999 );

	/// <summary>
	/// </summary>
	/// <param
	///     name="dateTime">
	/// </param>
	/// <returns></returns>
	public static DateTimeOffset GetStartOfDay( this DateTimeOffset dateTime ) => StartOfDay( dateTime );

	/// <summary>
	/// </summary>
	/// <param
	///     name="dateTime">
	/// </param>
	/// <returns></returns>
	public static DateTimeOffset GetEndOfDay( this DateTimeOffset dateTime ) => EndOfDay( dateTime );

	private static DateTimeOffset FixDateTime( this DateTimeOffset t ) => t < Epoch ? Epoch : t;

	public static TimeSpan TimeZoneToTimeSpan( this string timeZone )
	{
		var Tzi = TimeZoneInfo.FindSystemTimeZoneById( timeZone );
		return Tzi.GetUtcOffset( DateTime.UtcNow );
	}

	public static int TimeZoneToMinutesOffset( this string timeZone ) => (int)TimeZoneToTimeSpan( timeZone ).TotalMinutes;
	public static int TimeZoneToHoursOffset( this string timeZone ) => (int)TimeZoneToTimeSpan( timeZone ).TotalHours;
}

public class TimeRemaining
{
	private readonly long     InitialTicks;
	private          uint     CurrentSampleCount;
	private          DateTime LastSampleTime;

	private long TotalTicks;

	public TimeRemaining( int initialIntervalInSeconds )
	{
		InitialTicks = new TimeSpan( 0, 0, 0, initialIntervalInSeconds ).Ticks;
	}

	public TimeRemaining() : this( 3 ) // 3 seconds
	{
	}

	public TimeSpan Remaining( uint remainingItems )
	{
		var Now = DateTime.UtcNow;

		if( CurrentSampleCount == 0 )
		{
			LastSampleTime = Now;
			TotalTicks     = InitialTicks;
		}

		var ElapsedTicks = ( Now - LastSampleTime ).Ticks;

		TotalTicks += ElapsedTicks;

		LastSampleTime = Now;

		var AverageTicks = TotalTicks / ++CurrentSampleCount;

		var RemainingTicks = AverageTicks * remainingItems;

		return new TimeSpan( RemainingTicks );
	}

	public void Reset()
	{
		CurrentSampleCount = 0;
		TotalTicks         = 0;
	}

	public TimeSpan Remaining( int remainingItems ) => Remaining( (uint)remainingItems );

	public string ToString( uint remainingItems ) => Remaining( remainingItems ).ToString( @"hh\:mm\:ss" );

	public string ToString( int remainingItems ) => ToString( (uint)remainingItems );

	public string ToString( uint totalItems, uint currentItem ) => ToString( totalItems - currentItem );
	public string ToString( int totalItems, int currentItem ) => ToString( (uint)( totalItems - currentItem ) );
}