namespace Utils;

public static class AsyncExtensionMethods
{
	public static async Task<object> InvokeAsync( this MethodInfo methodInfo, object obj, params object[] parameters )
	{
		dynamic Awaitable = methodInfo.Invoke( obj, parameters )!;
		await Awaitable;
		return Awaitable.GetAwaiter().GetResult();
	}
}

public static class AsyncHelper
{
	private static readonly TaskFactory MyTaskFactory = new( CancellationToken.None,
	                                                         TaskCreationOptions.None,
	                                                         TaskContinuationOptions.None,
	                                                         TaskScheduler.Default );

	public static TResult RunSync<TResult>( Func<Task<TResult>> func ) => MyTaskFactory.StartNew( func )
	                                                                                   .Unwrap()
	                                                                                   .GetAwaiter()
	                                                                                   .GetResult();

	public static void RunSync( Func<Task> func )
	{
		MyTaskFactory.StartNew( func )
		             .Unwrap()
		             .GetAwaiter()
		             .GetResult();
	}
}