﻿using System.Collections.Concurrent;

namespace Utils;

public class SemaphoreQueue
{
	private readonly ConcurrentQueue<TaskCompletionSource<bool>> Queue = new();
	private readonly SemaphoreSlim                               Semaphore;

	public SemaphoreQueue( int initialCount )
	{
		Semaphore = new SemaphoreSlim( initialCount );
	}

	public SemaphoreQueue( int initialCount, int maxCount )
	{
		Semaphore = new SemaphoreSlim( initialCount, maxCount );
	}

	public void Wait()
	{
		WaitAsync().Wait();
	}

	public Task WaitAsync()
	{
		var TaskCompletionSource = new TaskCompletionSource<bool>();
		Queue.Enqueue( TaskCompletionSource );

		Semaphore.WaitAsync().ContinueWith( _ =>
		                                    {
			                                    if( Queue.TryDequeue( out var PoppedCompletionSource ) )
				                                    PoppedCompletionSource.SetResult( true );
		                                    } );
		return TaskCompletionSource.Task;
	}

	public void Release()
	{
		Semaphore.Release();
	}

#region SemaphoreQueueByTypeAndId
	private static readonly Dictionary<string, Dictionary<string, SemaphoreQueue>> SingeRequestLock = new();

	public static SemaphoreQueue SemaphoreQueueByTypeAndId<T>( string id, int maxThreads = 1 ) where T : class
	{
		SemaphoreQueue? LockObject;

		lock( SingeRequestLock )
		{
			if( !SingeRequestLock.TryGetValue( id, out var RequestLock ) )
				SingeRequestLock[ id ] = RequestLock = new Dictionary<string, SemaphoreQueue>();

			var Type = typeof( T ).FullName!;

			if( !RequestLock.TryGetValue( Type, out LockObject ) )
				RequestLock[ Type ] = LockObject = new SemaphoreQueue( maxThreads, maxThreads );
		}
		LockObject.Wait();
		return LockObject;
	}
#endregion
}