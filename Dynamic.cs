﻿using System.Dynamic;

namespace Utils;

public class DynamicDictionary : DynamicObject
{
	// This property returns the number of elements
	// in the inner dictionary.
	public int Count
	{
		get
		{
			lock( this )
				return Dictionary.Count;
		}
	}

	// The inner dictionary.
	private readonly Dictionary<string, object> Dictionary;

	public DynamicDictionary()
	{
		Dictionary = new Dictionary<string, object>();
	}

	public DynamicDictionary( DynamicDictionary dict )
	{
		lock( dict )
			Dictionary = new Dictionary<string, object>( dict.Dictionary );
	}

	// If you try to get a value of a property 
	// not defined in the class, this method is called.
	public override bool TryGetMember( GetMemberBinder binder, out object? result )
	{
		lock( this )
			return Dictionary.TryGetValue( binder.Name, out result );
	}

	// If you try to set a value of a property that is
	// not defined in the class, this method is called.
	public override bool TrySetMember( SetMemberBinder binder, object? value )
	{
		lock( this )
			Dictionary[ binder.Name ] = value!;

		return true;
	}

	public virtual bool TryGetValue( string name, out object? value )
	{
		lock( this )
			return Dictionary.TryGetValue( name, out value );
	}

	public object? this[ string name ]
	{
		get
		{
			lock( this )
			{
				if( !Dictionary.TryGetValue( name, out var Result ) )
					throw new ArgumentException( $"DynamicDictionary - Not Found: {name}" );

				return Result;
			}
		}

		set
		{
			lock( this )
				Dictionary[ name ] = value!;
		}
	}
}

public class DynamicDictionary<T> : DynamicDictionary
{
	public virtual bool TryGetValue( string name, out T value )
	{
		lock( this )
		{
			var Result = base.TryGetValue( name, out var Temp );

			if( Result && Temp is T Value )
			{
				value = Value;

				return true;
			}

			value = default!;

			return false;
		}
	}
}