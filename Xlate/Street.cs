﻿namespace Utils.Xlate;

public static class Street
{
	private static readonly Dictionary<string, string> StreetTypeAbbreviations = new()
	                                                                             {
		                                                                             { "RD", "ROAD" },
		                                                                             { "ST", "STREET" },
		                                                                             { "AVE", "AVENUE" },
		                                                                             { "BLVD", "BOULEVARD" },
		                                                                             { "DR", "DRIVE" },
		                                                                             { "LN", "LANE" },
		                                                                             { "CT", "COURT" },
		                                                                             { "PL", "PLACE" },
		                                                                             { "CIR", "CIRCLE" },
		                                                                             { "HWY", "HIGHWAY" },
		                                                                             { "PKWY", "PARKWAY" },
		                                                                             { "SQ", "SQUARE" },
		                                                                             { "CL", "CLOSE" },
		                                                                             { "PRD", "PARADE" },
		                                                                             { "PDE", "PARADE" }
	                                                                             };


	public static string FixAbbreviation( this string street )
	{
		var StreetParts = street.Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

		var LengthM1 = StreetParts.Length - 1;

		if( LengthM1 >= 0 )
		{
			var StreetType = StreetParts[ LengthM1 ].ToUpper();

			if( StreetTypeAbbreviations.TryGetValue( StreetType, out var Abbreviation ) )
				StreetParts[ LengthM1 ] = Abbreviation;

			return string.Join( ' ', StreetParts );
		}
		return street;
	}
}