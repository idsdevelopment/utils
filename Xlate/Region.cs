﻿namespace Utils.Xlate;

public static class Region
{
	public enum RESULT_TYPE
	{
		NONE,
		ABBREVIATION,
		NAME
	}

	private static readonly Dictionary<string, string> AustraliaStateAbbreviations = new()
	                                                                                 {
		                                                                                 { "NEW SOUTH WALES", "NSW" },
		                                                                                 { "VICTORIA", "VIC" },
		                                                                                 { "QUEENSLAND", "QLD" },
		                                                                                 { "SOUTH AUSTRALIA", "SA" },
		                                                                                 { "WESTERN AUSTRALIA", "WA" },
		                                                                                 { "TASMANIA", "TAS" },
		                                                                                 { "NORTHERN TERRITORY", "NT" },
		                                                                                 { "AUSTRALIAN CAPITAL TERRITORY", "ACT" }
	                                                                                 };


	private static readonly Dictionary<string, string> CanadaProvinceAbbreviations = new()
	                                                                                 {
		                                                                                 { "ALBERTA", "AB" },
		                                                                                 { "BRITISH COLUMBIA", "BC" },
		                                                                                 { "MANITOBA", "MB" },
		                                                                                 { "NEW BRUNSWICK", "NB" },
		                                                                                 { "NEWFOUNDLAND AND LABRADOR", "NL" },
		                                                                                 { "NOVA SCOTIA", "NS" },
		                                                                                 { "ONTARIO", "ON" },
		                                                                                 { "PRINCE EDWARD ISLAND", "PE" },
		                                                                                 { "QUEBEC", "QC" },
		                                                                                 { "SASKATCHEWAN", "SK" },
		                                                                                 { "NORTHWEST TERRITORIES", "NT" },
		                                                                                 { "NUNAVUT", "NU" },
		                                                                                 { "YUKON", "YT" }
	                                                                                 };


	private static readonly Dictionary<string, string> UsStateAbbreviations = new()
	                                                                          {
		                                                                          { "ALABAMA", "AL" },
		                                                                          { "ALASKA", "AK" },
		                                                                          { "ARIZONA", "AZ" },
		                                                                          { "ARKANSAS", "AR" },
		                                                                          { "CALIFORNIA", "CA" },
		                                                                          { "COLORADO", "CO" },
		                                                                          { "CONNECTICUT", "CT" },
		                                                                          { "DELAWARE", "DE" },
		                                                                          { "FLORIDA", "FL" },
		                                                                          { "GEORGIA", "GA" },
		                                                                          { "HAWAII", "HI" },
		                                                                          { "IDAHO", "ID" },
		                                                                          { "ILLINOIS", "IL" },
		                                                                          { "INDIANA", "IN" },
		                                                                          { "IOWA", "IA" },
		                                                                          { "KANSAS", "KS" },
		                                                                          { "KENTUCKY", "KY" },
		                                                                          { "LOUISIANA", "LA" },
		                                                                          { "MAINE", "ME" },
		                                                                          { "MARYLAND", "MD" },
		                                                                          { "MASSACHUSETTS", "MA" },
		                                                                          { "MICHIGAN", "MI" },
		                                                                          { "MINNESOTA", "MN" },
		                                                                          { "MISSISSIPPI", "MS" },
		                                                                          { "MISSOURI", "MO" },
		                                                                          { "MONTANA", "MT" },
		                                                                          { "NEBRASKA", "NE" },
		                                                                          { "NEVADA", "NV" },
		                                                                          { "NEW HAMPSHIRE", "NH" },
		                                                                          { "NEW JERSEY", "NJ" },
		                                                                          { "NEW MEXICO", "NM" },
		                                                                          { "NEW YORK", "NY" },
		                                                                          { "NORTH CAROLINA", "NC" },
		                                                                          { "NORTH DAKOTA", "ND" },
		                                                                          { "OHIO", "OH" },
		                                                                          { "OKLAHOMA", "OK" },
		                                                                          { "OREGON", "OR" },
		                                                                          { "PENNSYLVANIA", "PA" },
		                                                                          { "RHODE ISLAND", "RI" },
		                                                                          { "SOUTH CAROLINA", "SC" },
		                                                                          { "SOUTH DAKOTA", "SD" },
		                                                                          { "TENNESSEE", "TN" },
		                                                                          { "TEXAS", "TX" },
		                                                                          { "UTAH", "UT" },
		                                                                          { "VERMONT", "VT" },
		                                                                          { "VIRGINIA", "VA" },
		                                                                          { "WASHINGTON", "WA" },
		                                                                          { "WEST VIRGINIA", "WV" },
		                                                                          { "WISCONSIN", "WI" },
		                                                                          { "WYOMING", "WY" },
		                                                                          { "DISTRICT OF COLUMBIA", "DC" }
	                                                                          };

#region Country Codes
	// Must be in the same order as the CountryCodes array
	private enum COUNTRY
	{
		AUSTRALIA,
		CANADA,
		UNITED_STATES
	}
#endregion

	private class AbbreviationDictionary
	{
		private readonly IDictionary<string, string> AbbreviationsToName;
		private readonly Dictionary<string, string>  NamesToAbbreviations;

		// ReSharper disable once UnusedMember.Local
		public bool TryGetName( string abbreviation, out string? name )
		{
			abbreviation = abbreviation.ToUpperInvariant();
			return AbbreviationsToName.TryGetValue( abbreviation, out name );
		}

		// ReSharper disable once UnusedMember.Local
		public bool TryGetAbbreviation( string name, out string? abbreviation )
		{
			name = name.ToUpperInvariant();
			return NamesToAbbreviations.TryGetValue( name, out abbreviation );
		}

		public RESULT_TYPE TryGetValue( string key, out string? value )
		{
			key = key.ToUpperInvariant();

			if( AbbreviationsToName.TryGetValue( key, out value ) )
				return RESULT_TYPE.ABBREVIATION;

			if( NamesToAbbreviations.TryGetValue( key, out value ) )
				return RESULT_TYPE.NAME;

			value = null;
			return RESULT_TYPE.NONE;
		}

		public AbbreviationDictionary( IDictionary<string, string> abbreviations )
		{
			AbbreviationsToName  = abbreviations;
			NamesToAbbreviations = abbreviations.ToDictionary( kvp => kvp.Value, kvp => kvp.Key );
		}
	}

#region Accessors
	public static RESULT_TYPE TryGetVale( string country, string region, out string? value )
	{
		country = country.ToUpperInvariant();

		if( !CountryAbbreviationXlate.TryGetValue( country, out var CountryEnum ) )
		{
			value = null;
			return RESULT_TYPE.NONE;
		}

		return CountryDictionaries[ (int)CountryEnum ].TryGetValue( region, out value );
	}
#endregion

#region Country Disctionaries
	// Must be in the same order as the COUNTRY enum
	private static readonly AbbreviationDictionary[] CountryDictionaries =
	{
		new( AustraliaStateAbbreviations ),
		new( CanadaProvinceAbbreviations ),
		new( UsStateAbbreviations )
	};


	private static readonly Dictionary<string, COUNTRY> CountryAbbreviationXlate = new()
	                                                                               {
		                                                                               { "AU", COUNTRY.AUSTRALIA },
		                                                                               { "AUS", COUNTRY.AUSTRALIA },
		                                                                               { "AUSTRALIA", COUNTRY.AUSTRALIA },
		                                                                               { "CA", COUNTRY.CANADA },
		                                                                               { "CAN", COUNTRY.CANADA },
		                                                                               { "CANADA", COUNTRY.CANADA },
		                                                                               { "US", COUNTRY.UNITED_STATES },
		                                                                               { "USA", COUNTRY.UNITED_STATES },
		                                                                               { "UNITED STATES", COUNTRY.UNITED_STATES }
	                                                                               };

	public static void FixupCountryRegion( string country, string region,
	                                       ref string countryName, ref string countryCode,
	                                       ref string regionName, ref string regionCode )
	{
		country = country.ToUpperInvariant();

		if( !CountryAbbreviationXlate.TryGetValue( country, out var CountryEnum ) )
			return;

		( countryName, countryCode ) = CountryEnum switch
		                               {
			                               COUNTRY.AUSTRALIA     => ( "AUSTRALIA", "AU" ),
			                               COUNTRY.CANADA        => ( "CANADA", "CA" ),
			                               COUNTRY.UNITED_STATES => ( "UNITED STATES", "US" ),
			                               _                     => ( country, country )
		                               };

		region = region.ToUpperInvariant();

		var Result = CountryDictionaries[ (int)CountryEnum ].TryGetValue( region, out var RegionNameOrAbbreviation );

		if( Result != RESULT_TYPE.NONE )
		{
			switch( Result )
			{
			case RESULT_TYPE.ABBREVIATION:
				regionCode = RegionNameOrAbbreviation!;
				regionName = region;
				break;

			default:
				regionName = RegionNameOrAbbreviation!;
				regionCode = region;
				break;
			}
		}
	}
#endregion
}