﻿#nullable enable

// ReSharper disable ExplicitCallerInfoArgument

namespace Utils.Windows;

public class Logging : IDisposable
{
	public const string LOG_FILE_NAME           = "Ids2ClientLog.Txt",
	                    LOG_FILE_SEARCH_PATTERN = "*-" + LOG_FILE_NAME,
	                    IDS                     = "Ids",
	                    DATE_TIME_FORMAT        = "yyyy-MM-dd-hh-mm-ss";

	public const uint RETENTION_DAYS = 30;

	public string Log => Writer?.Log ?? "";

	private readonly TextWriter OldOutput;

	public  Action<Exception>? OnUnhandledException;
	private LogWriter?         Writer;

	public Logging( string logFileName = LOG_FILE_NAME, uint retentionDays = RETENTION_DAYS )
	{
		var DocsDir = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ), IDS );
		Directory.CreateDirectory( DocsDir );

		Task.Run( () =>
		          {
			          var Now        = DateTime.Now;
			          var Day30Ticks = new TimeSpan( (int)retentionDays, 0, 0, 0 ).Ticks;

			          // Clean up old log files
			          var Files = Directory.GetFiles( DocsDir, LOG_FILE_SEARCH_PATTERN );

			          foreach( var F in Files )
			          {
				          var FName = Path.GetFileName( F );
				          var P     = FName.LastIndexOf( '-' );

				          if( P > 0 )
				          {
					          var TimeStr = FName.Substring( 0, P );

					          try
					          {
						          // DATE_TIME_FORMAT = "yyyy-MM-dd-hh-mm-ss"
						          var Date = DateTime.ParseExact( TimeStr, "yyyy-MM-dd", CultureInfo.InvariantCulture );

						          if( ( Now - Date ).Ticks > Day30Ticks )
							          System.IO.File.Delete( F );
					          }
					          catch
					          {
					          }
				          }
			          }
		          } );

		OldOutput = Console.Out;

		LogWriter NewWriter()
		{
			//var DocsFileName = Path.Combine( DocsDir, $"{DateTime.Now:yyyy-MM-dd-hh-mm-ss}-{logFileName}" );
			var DocsFileName = Path.Combine( DocsDir, $"{DateTime.Now:yyyy-MM-dd}-{logFileName}" );

			return Writer = new LogWriter( DocsFileName, OldOutput );
		}

		Console.SetOut( NewWriter() );

		AppDomain.CurrentDomain.UnhandledException += ( _, args ) =>
		                                              {
			                                              if( Writer is not null )
			                                              {
				                                              var E = (Exception)args.ExceptionObject;
				                                              Console.WriteLine( $@"Unhandled exception : {E.Message}" );
				                                              var Terminated = args.IsTerminating ? "Yes" : "No";
				                                              Console.WriteLine( $@"Application terminated: {Terminated}" );
				                                              Writer.Flush();

				                                              if( args.IsTerminating )
					                                              Writer.Close();

				                                              Task.Run( () =>
				                                                        {
					                                                        OnUnhandledException?.Invoke( E );
				                                                        } );

				                                              Thread.Sleep( 5000 );
			                                              }
		                                              };
	}

	/// <summary>
	///     cjt Utility method to write out a line in the current log.
	/// </summary>
	/// <param
	///     name="message">
	/// </param>
	/// <param
	///     name="methodName">
	/// </param>
	/// <param
	///     name="className">
	/// </param>
	public static void WriteLogLine( string message, string methodName, string className )
	{
		var Now  = DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.ffff" );
		var Line = Now + "; " + message + "; Method:" + methodName + "; Class:" + className;
		Console.WriteLine( Line );
	}

	public static void WriteLogLine( string message,
	                                 [CallerMemberName] string memberName = "",
	                                 [CallerLineNumber] int sourceLineNumber = 0,
	                                 [CallerFilePath] string sourceFilePath = "" )
	{
		sourceFilePath = sourceFilePath.Substring( sourceFilePath.LastIndexOf( '\\' ) + 1 );
		var Now  = DateTime.Now.ToString( "yyyy/MM/dd HH:mm:ss.ffff" );
		var Line = Now + "; " + message + "; Method:" + memberName + "; File:" + sourceFilePath + "; Line:" + sourceLineNumber;

		for( var I = 10; I > 0; I-- )
		{
			try
			{
				Console.WriteLine( Line );
				break;
			}
			catch // Probably in use by another thread
			{
				Thread.Sleep( 100 );
			}
		}
	}

	public static void WriteLogLine( Exception e,
	                                 [CallerMemberName] string memberName = "",
	                                 [CallerLineNumber] int sourceLineNumber = 0,
	                                 [CallerFilePath] string sourceFilePath = "" )
	{
		while( e is not null )
		{
			WriteLogLine( e.Message, memberName, sourceLineNumber, sourceFilePath );

			if( e.InnerException is not null )
				e = e.InnerException;
		}
	}

	private class LogWriter : TextWriter
	{
		public override Encoding Encoding { get; } = Encoding.ASCII;

		internal string Log
		{
			get
			{
				while( true )
				{
					try
					{
						lock( LockObject )
						{
							OutputStream.Flush();
							FileStream.Flush();
						}

						break;
					}
					catch // Probably in use by another thread
					{
						Thread.Sleep( 100 );
					}
				}

				var P = FileStream.Position;

				try
				{
					FileStream.Position = 0;
					var L      = FileStream.Length;
					var Buffer = new byte[ L ];
					// ReSharper disable once MustUseReturnValue
					FileStream.Read( Buffer, 0, (int)L );

					return Encoding.GetString( Buffer );
				}
				finally
				{
					FileStream.Position = P;
				}
			}
		}

		private readonly FileStream   FileStream;
		private readonly object       LockObject = new();
		private readonly TextWriter   OldWriter;
		private readonly StreamWriter OutputStream;

		public LogWriter( string logFileName, TextWriter oldWriter )
		{
			OldWriter = oldWriter;

			// Needs a FileStream so it can send the crash log to the Azure server
			OutputStream = new StreamWriter( FileStream = new FileStream( logFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read ) );
			FileStream.Seek( 0, SeekOrigin.End ); // Append
		}

		public override void Write( char value )
		{
			lock( LockObject ) // If already locked, then another thread is writing to the log file
			{
				OldWriter.Write( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.Write( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}
		}

		public override void Write( string? value )
		{
			lock( LockObject )
			{
				OldWriter.Write( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.Write( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}
		}

		public override void WriteLine()
		{
			lock( LockObject )
			{
				OldWriter.WriteLine();
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.WriteLine();
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}
		}

		public override void WriteLine( string? value )
		{
			lock( LockObject )
			{
				OldWriter.WriteLine( value );
				OldWriter.Flush();

				Task.Run( () =>
				          {
					          lock( LockObject )
					          {
						          try
						          {
							          OutputStream.WriteLine( value );
							          OutputStream.Flush();
						          }
						          catch
						          {
						          }
					          }
				          } );
			}
		}

		public override void Close()
		{
			lock( LockObject )
				OutputStream.Close();
		}
	}

	public void Dispose()
	{
		Writer?.Close();
		Console.SetOut( OldOutput );
	}
}