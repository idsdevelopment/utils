﻿using Newtonsoft.Json;

namespace Utils.Csv;

public class Csv<T> : Csv
{
	private const char TOKEN = char.MaxValue;


	public Csv( string csvData ) : base( csvData )
	{
	}

	public Csv( Stream csvData ) : base( csvData )
	{
	}

	public static List<T> Deserialise( string csvData ) => new Csv<T>( csvData ).Deserialise();
	public static List<T> Deserialise( string csvData, Dictionary<string, string> map ) => new Csv<T>( csvData ).Deserialise( map );

	public static List<T> Deserialise( Stream csvData ) => new Csv<T>( csvData ).Deserialise();
	public static List<T> Deserialise( Stream csvData, Dictionary<string, string> map ) => new Csv<T>( csvData ).Deserialise( map );

	public static List<T> DeserialiseFromFile( string fileName ) => GetCsvFromFile( fileName ).Deserialise();
	public static List<T> DeserialiseFromFile( string fileName, Dictionary<string, string> map ) => GetCsvFromFile( fileName ).Deserialise( map );


	public List<T> Deserialise()
	{
		var Rc = RowCount;

		if( Rc > 0 )
		{
			var Row0  = this[ 0 ];
			var Count = Row0.CellCount;

			var ColumnMap = new string[ Count ];

			for( var Column = 0; Column < Count; Column++ )
				ColumnMap[ Column ] = Row0[ Column ].AsString;

			//Start List
			var Json  = new StringBuilder( "[" );
			var First = true;

			for( var Row = 1; Row < Rc; Row++ )
			{
				var R = this[ Row ];

				if( !First )
					Json.Append( ',' );
				else
					First = false;

				// Start Object
				Json.Append( '{' );

				var ObjectFirst = true;

				for( var Column = 0; Column < R.CellCount; Column++ )
				{
					if( !ObjectFirst )
						Json.Append( ',' );
					else
						ObjectFirst = false;

					var Text = R[ Column ].AsString.Replace( '"', TOKEN ).Replace( TOKEN.ToString(), "\"" )
					                      .Replace( '\\', TOKEN ).Replace( TOKEN.ToString(), "\\\\" );

					Json.Append( $"\"{ColumnMap[ Column ]}\":\"{Text}\"" );
				}

				// End Object
				Json.Append( '}' );
			}

			//End List
			Json.Append( ']' );

			return JsonConvert.DeserializeObject<List<T>>( Json.ToString() ) ?? new List<T>();
		}

		return new List<T>();
	}

	public List<T> Deserialise( Dictionary<string, string> map )
	{
		if( RowCount > 0 )
		{
			var Row   = this[ 0 ];
			var Count = Row.CellCount;

			for( var Column = 0; Column < Count; Column++ )
			{
				var Header = Row[ Column ].AsString.Trim();

				if( map.TryGetValue( Header, out var FieldName ) )
					Row[ Column ] = FieldName;
			}

			return Deserialise();
		}
		return new List<T>();
	}

	private static Csv<T> GetCsvFromFile( string fileName )
	{
		using var Stream = new FileStream( fileName, FileMode.Open, FileAccess.Read, FileShare.None );
		return new Csv<T>( Stream );
	}
}