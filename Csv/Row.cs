﻿namespace Utils.Csv;

public class OrderedDictionary<TIndex, TData> : List<TData> where TData : new() where TIndex : notnull
{
	private readonly Dictionary<TIndex, TData> Dict = new();

	public void Add( TIndex index, TData data )
	{
		Add( data );
		Dict.Add( index, data );
	}

	public bool ContainsKey( TIndex index ) => Dict.ContainsKey( index );

	public void RemoveAt( TIndex ndx )
	{
		if( ContainsKey( ndx ) )
		{
			var Data = Dict[ ndx ];
			Dict.Remove( ndx );
			base.RemoveAt( IndexOf( Data ) );
		}
	}

	public TData this[ TIndex index ]
	{
		get
		{
			if( !Dict.TryGetValue( index, out var Data ) )
				this[ index ] = Data = new TData();
			return Data;
		}

		set
		{
			if( ContainsKey( index ) )
			{
				var Old = Dict[ index ];
				var Ndx = IndexOf( Old );
				base[ Ndx ]   = value;
				Dict[ index ] = value;
			}
			else
				Add( index, value );
		}
	}
}

public class Row : List<Cell>
{
	internal int CellCount => Count;

	private void AddCells( int index )
	{
		for( var C = Count; C <= index; C++ )
			Add( new Cell() );
	}

	internal string AsFileString( int totalColumns )
	{
		int I,
		    C;
		var First  = true;
		var Result = new StringBuilder();

		for( I = 0, C = Count; I < C; I++ )
		{
			if( !First )
				Result.Append( (char)Csv.TOKENS.COMMA );
			else
				First = false;

			Result.Append( base[ I ].AsFileString );
		}

		for( ; I++ < totalColumns; )
			Result.Append( (char)Csv.TOKENS.COMMA );

		return Result.ToString();
	}

	public new Cell this[ int index ]
	{
		get
		{
			AddCells( index );
			return base[ index ];
		}

		set
		{
			AddCells( index );
			base[ index ] = value;
		}
	}
}

public class Rows : OrderedDictionary<int, Row>
{
	internal int Columns
	{
		get
		{
			var Max = 0;

			foreach( var Row in this )
				Max = Math.Max( Row.CellCount, Max );

			return Max;
		}
	}

	internal string AsFileString
	{
		get
		{
			var Result = new StringBuilder();

			var MaxCols = Columns;

			foreach( var Rows in this )
			{
				var Data = Rows.AsFileString( MaxCols ) + "\r\n";
				Result.Append( Data );
			}

			return Result.ToString();
		}
	}

	public int AppendRow()
	{
		var NewRow = Count;
		AddRow( NewRow, new Row() );

		return NewRow;
	}

	internal void AddRow( int ndx, Row row )
	{
		if( ContainsKey( ndx ) )
			this[ ndx ] = row;
		else
			Add( ndx, row );
	}

	internal void AddRow( int ndx, string row )
	{
		var Cells = row.Split( (char)Csv.TOKENS.COMMA );

		var NewRow = new Row();
		NewRow.AddRange( Cells.Select( cellContent => new Cell { AsString = cellContent } ) );

		AddRow( ndx, NewRow );
	}
}