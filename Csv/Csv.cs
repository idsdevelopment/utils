﻿namespace Utils.Csv;

public static class CsvExtensions
{
	public static void Write( this Csv csvData, Stream stream, Encoding? encoding = null )
	{
		Csv.Write( csvData, stream, encoding );
	}

	public static void WriteToFile( this Csv csvData, string fileName, Encoding? encoding = null )
	{
		Csv.WriteToFile( csvData, fileName, encoding );
	}
}

public class Csv : IEnumerable
{
	public int Rows => _Rows.Count;

	public int RowCount => _Rows.Count;

	private readonly Rows _Rows = [];

	public Csv()
	{
	}

	public Csv( byte[] bytes ) : this( bytes.ToUnicodeString() )
	{
	}

	public Csv( string csvData )
	{
		var StringData = new StringBuilder( csvData.TrimEnd( '\r', '\n' ) );

		// Clean Up Lines
		StringData.Replace( "\r\n", "\n" );
		StringData.Replace( "\r", "" );

		// Fix Empty Cells
		StringData.Replace( ",\"\",", ",," );
		StringData.Replace( "\"\"\n", "\n" );

		var Len = StringData.Length;

		var StringData2 = new StringBuilder();
		var InQuote     = false;

		// Tokenise commas in quotes
		for( var I = 0; I < Len; I++ )
		{
			var C = StringData[ I ];

			// Beginning or end of a quote sequence (Loose the quotes)
			if( C == '"' )
			{
				if( InQuote )
				{
					// Look ahead for a Double Quote (Escaped Quote)
					var I1 = I + 1;
					var C1 = I1 < Len ? StringData[ I1 ] : '\n';

					if( C1 == '"' )
					{
						StringData2.Append( C1 );
						I = I1;
					}
					else
						InQuote = false;
				}
				else
					InQuote = true;
			}
			else
			{
				if( ( C == ',' ) && InQuote )
					C = (char)TOKENS.COMMA_IN_QUOTE;

				StringData2.Append( C );
			}
		}

		// Put Cell Commas and Quotes Back

		StringData2.Replace( ',', (char)TOKENS.COMMA );
		StringData2.Replace( (char)TOKENS.COMMA_IN_QUOTE, ',' );

		var Lines = StringData2.ToString().TrimEnd().Split( '\n' );

		_Rows = [];

		Len = Lines.Length;

		for( var I = 0; I < Len; I++ )
			_Rows.AddRow( I, Lines[ I ] );
	}

	public Csv( Stream csvData ) : this( StreamToString( csvData ) )
	{
	}

	protected static string StreamToString( Stream stream )
	{
		var Len = stream.Length;

		if( Len > 0 )
		{
			var Buffer = new byte[ Len ];

			stream.Position = 0;

			var Ndx = 0;

			do
			{
				var ReadLen = stream.Read( Buffer, Ndx, (int)Len );
				Ndx     += ReadLen;
				Len     -= ReadLen;
			}
			while( Len > 0 );

			return Buffer.ToUnicodeString();
		}
		return "";
	}

	internal enum TOKENS : ushort
	{
		COMMA_IN_QUOTE = ushort.MaxValue,
		COMMA          = COMMA_IN_QUOTE - 1
	}

	public void RemoveAt( int ndx )
	{
		_Rows.RemoveAt( ndx );
	}

	public int AppendRow() => _Rows.AppendRow();

	public void Sort( Comparison<Row> func )
	{
		_Rows.Sort( func );
	}

	public string ToString( char separator ) => _Rows.AsFileString.Replace( (char)TOKENS.COMMA, separator );

	public override string ToString() => ToString( ',' );

	public static Csv Read( string csvData ) => new( csvData );

	public static Csv Read( Stream csvData ) => new( csvData );

	public static Csv ReadFromFile( string fileName )
	{
		using var Stream = new FileStream( fileName, FileMode.Open, FileAccess.Read, FileShare.None );

		return new Csv( Stream );
	}

	public static void Write( Csv csvData, Stream stream, Encoding? encoding = null )
	{
		encoding ??= Encoding.UTF8;

	#if NET5_0_OR_GREATER
		using var Writer = new StreamWriter( stream, encoding, leaveOpen: true );
	#else
		using var Writer = new StreamWriter( stream, encoding );
	#endif
		stream.Position = 0;

		var Data = csvData.ToString();
		Writer.Write( Data );
		Writer.Flush();
		Writer.Close();
	}

	public static void WriteToFile( Csv csvData, string fileName, Encoding? encoding = null )
	{
		using var Stream = new FileStream( fileName, FileMode.Create, FileAccess.Write, FileShare.None );

		Write( csvData, Stream, encoding );
		Stream.Close();
	}

	public bool IsRowBlank( int ndx )
	{
		var Cols = this[ ndx ];

		foreach( var Col in Cols )
		{
			if( Col.AsString.IsNotNullOrWhiteSpace() )
				return false;
		}
		return true;
	}

	public Csv TrimEnd()
	{
		for( var Ndx = RowCount; --Ndx >= 0; )
		{
			if( !IsRowBlank( Ndx ) )
				break;

			RemoveAt( Ndx );
		}
		return this;
	}

	public Row this[ int ndx ] => _Rows[ ndx ];

	public IEnumerator GetEnumerator() => _Rows.GetEnumerator();
}