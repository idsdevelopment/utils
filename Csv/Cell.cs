﻿namespace Utils.Csv;

public class Cell
{
	public string AsString
	{
		get => ToString();

		set
		{
			Type           = TYPE.STRING;
			_StringContent = value;
		}
	}

	public double AsDouble
	{
		get => ToDouble();

		set
		{
			Type           = TYPE.DOUBLE;
			_DoubleContent = value;
		}
	}


	public decimal AsDecimal
	{
		get => ToDecimal();

		set
		{
			Type            = TYPE.DECIMAL;
			_DecimalContent = value;
		}
	}

	internal string AsFileString
	{
		get
		{
			var SaveType = Type;

			var Value = AsString;
			Value = Value.Replace( "\"", "\"\"" );
			Type  = SaveType;

			return Type != TYPE.STRING ? Value : $"\"{Value}\"";
		}
	}

	private decimal _DecimalContent;
	private double  _DoubleContent;

	private string _StringContent = "";

	private TYPE Type = TYPE.STRING;

	public static implicit operator string( Cell c ) => c.AsString;

	public static implicit operator Cell( string s ) => new() { _StringContent = s };


	public static implicit operator double( Cell c ) => c.AsDouble;

	public static implicit operator Cell( double d ) => new() { _DoubleContent = d, Type = TYPE.DOUBLE };


	public static implicit operator decimal( Cell c ) => c.AsDecimal;

	public static implicit operator Cell( decimal d ) => new() { _DecimalContent = d, Type = TYPE.DECIMAL };

	private enum TYPE
	{
		STRING,
		DOUBLE,
		DECIMAL
	}

	private double ToDouble()
	{
		double Result;

		switch( Type )
		{
		case TYPE.DOUBLE:
			return _DoubleContent;

		case TYPE.STRING:
			if( !double.TryParse( _StringContent, out Result ) )
				Result = 0;
			break;

		case TYPE.DECIMAL:
			Result = (double)_DecimalContent;
			break;

		default:
			Result = 0;
			break;
		}

		Type = TYPE.DOUBLE;
		return _DoubleContent = Result;
	}

	private decimal ToDecimal()
	{
		decimal Result;

		switch( Type )
		{
		case TYPE.DECIMAL:
			return _DecimalContent;

		case TYPE.DOUBLE:
			Result = (decimal)_DoubleContent;
			break;

		case TYPE.STRING:
			if( !decimal.TryParse( _StringContent, out Result ) )
				Result = 0;
			break;

		default:
			Result = 0;
			break;
		}

		Type = TYPE.DECIMAL;
		return _DecimalContent = Result;
	}

	private new string ToString()
	{
		string Result;

		switch( Type )
		{
		case TYPE.STRING:
			return _StringContent;

		case TYPE.DECIMAL:
			Result = _DecimalContent.ToString( CultureInfo.InvariantCulture );
			break;

		case TYPE.DOUBLE:
			Result = _DoubleContent.ToString( CultureInfo.InvariantCulture );
			break;

		default:
			Result = "";
			break;
		}

		Type = TYPE.STRING;
		return _StringContent = Result;
	}
}