﻿namespace Utils;

public static class Lists
{
	public static List<List<T>> Chunks<T>( this IList<T> list, int chunkSize )
	{
		var Result = new List<List<T>>();
		var C      = list.Count;

		for( var I = 0; I < C; )
		{
			var Temp = new List<T>();
			Result.Add( Temp );

			for( var Chk = chunkSize; ( Chk > 0 ) && ( I < C ); --Chk, ++I )
				Temp.Add( list[ I ] );
		}

		return Result;
	}

	public static List<List<T>> Chunks<T>( this IEnumerable<T> list, int chunkSize ) => new List<T>( list ).Chunks( chunkSize );

	public static IList<T> Shuffle<T>( this IList<T> list )
	{
		var Random = new Random();
		var N      = list.Count;

		while( N > 1 )
		{
			var K = Random.Next( N-- );
			( list[ K ], list[ N ] ) = ( list[ N ], list[ K ] );
		}
		return list;
	}
}