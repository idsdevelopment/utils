﻿namespace Utils;

public abstract class ADisposable : IDisposable
{
	private bool Disposed;

	protected abstract void OnDispose( bool systemDisposing );

	~ADisposable()
	{
		if( !Disposed )
			DoDispose( false );
	}

	private void DoDispose( bool systemDisposing )
	{
		try
		{
			if( !Disposed )
			{
				Disposed = true;
				OnDispose( systemDisposing );
			}
		}
		catch
		{
		}
	}

#region IDisposable implementation
	public void Dispose()
	{
		DoDispose( true );

		// This object will be cleaned up by the Dispose method. 
		// Therefore, you should call GC.SupressFinalize to 
		// take this object off the finalization queue 
		// and prevent finalization code for this object 
		// from executing a second time.

		GC.SuppressFinalize( this );
	}
#endregion
}