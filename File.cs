﻿namespace Utils;

public class File
{
	private static readonly string[] InvalidFileParts =
	{
		@"\",
		":",
		"*",
		"?",
		"\"",
		"<",
		">",
		"|",
		"CON",
		"PRN",
		"AUX",
		"CLOCK$",
		"NUL",
		"COM0",
		"COM1",
		"COM2",
		"COM3",
		"COM4",
		"COM5",
		"COM6",
		"COM7",
		"COM8",
		"COM9",
		"LPT0",
		"LPT1",
		"LPT2",
		"LPT3",
		"LPT4",
		"LPT5",
		"LPT6",
		"LPT7",
		"LPT8",
		"LPT9"
	};

	public static string MakeValidFileName( string fName, string replacement = "_" )
	{
		return InvalidFileParts.Aggregate( fName, ( current, invalid ) => current.Replace( invalid, replacement ) );
	}

	public static string MakeIdentFileName( string fName, char replacement = '_', bool allowDot = false )
	{
		var L = fName.Length;

		var Result = new StringBuilder();

		for( var I = 0; I < L; )
		{
			var C = fName[ I++ ];

			switch( C )
			{
			case '.':
				Result.Append( allowDot ? C : replacement );
				break;

			case (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or (>= '0' and <= '9') or '_':
				Result.Append( C );
				break;

			default:
				Result.Append( replacement );
				break;
			}
		}

		return MakeValidFileName( Result.ToString() );
	}

	public static string AddPathSeparator( string fullPath ) => fullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar )
	                                                            + Path.DirectorySeparatorChar;
}