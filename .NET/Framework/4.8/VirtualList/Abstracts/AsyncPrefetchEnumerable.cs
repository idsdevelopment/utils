﻿using System.Collections.Concurrent;

// ReSharper disable StaticMemberInGenericType

namespace Utils.VirtualList.Abstracts;

internal abstract class AsyncPrefetchEnumerable<T> : IEnumerable<T>, IEnumerator<T> // IAsyncEnumerable<T>, IAsyncEnumerator<T>,
{
	protected AsyncPrefetchEnumerable( int preFetchCount, CancellationToken cancellationToken = new() )
	{
		PreFetchCount = preFetchCount;

		Task.Run( async () =>
				  {
					  var Count = await TotalCount();

					  bool DoFetch;

					  lock( Items )
					  {
						  DoFetch    = Count != TotalItems;
						  TotalItems = Count;
					  }

					  if( DoFetch )
						  await PreFetchNext( 0, cancellationToken );
				  } );
	}

	public Task<T?> this[ int index, CancellationToken cancellationToken = new() ] => PreFetchItem( index, cancellationToken );

	private static readonly object LockObject = new();

	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	public void Reset()
	{
		lock( LockObject )
			Index = -1;
	}

	public void Dispose()
	{
	}

	public IEnumerator<T> GetEnumerator() => this;

#region MoveNext
	// public ValueTask<bool> MoveNextAsync() => new(MoveNext());

	private readonly SemaphoreSlim MoveSemaphoreSlim = new( 1, 1 );

	public bool MoveNext()
	{
		MoveSemaphoreSlim.Wait();

		try
		{
			bool Ok;

			lock( LockObject )
			{
				Ok = ++Index < TotalItems;

				if( Ok )
				{
					Ok = Items.TryGetValue( Index, out var Tuple );

					if( Ok )
					{
						Current = Tuple.Item!;
						return Ok;
					}
				}
			}

			try
			{
				var Tsks = new[]
						   {
							   Task.Run( async () =>
									     {
										     Current = ( await PreFetchItem( Index, new CancellationToken() ) )!;
									     } )
						   };
				Task.WaitAll( Tsks );
				Ok = Index < TotalItems;
			}
			catch
			{
				Ok = false;
			}
			return Ok;
		}
		finally
		{
			var Upper = Items.Count;

			if( !InPreFetchNext && ( ( Upper - Index ) < PreFetchCount ) )
			{
				Tasks.RunVoid( async () =>
							   {
								   await PreFetchNext( Upper, new CancellationToken() );
								   MoveSemaphoreSlim.Release();
							   } );
			}
			else
				MoveSemaphoreSlim.Release();
		}
	}
#endregion

	/*
	public ValueTask DisposeAsync() => new(Task.Run(Dispose));


	public IAsyncEnumerator<T> GetAsyncEnumerator(CancellationToken cancellationToken = new()) => this;
	*/

#region Current
	public T Current { get; private set; } = default!;

	object IEnumerator.Current => Current!;
#endregion


#region Items
	private int Index;

	private static int TotalItems;

	private static readonly ConcurrentDictionary<int, (DateTime LastUpdatedUTC, T? Item)> Items = new();
#endregion

#region PreFetch
	private readonly int PreFetchCount;

	private async Task<T?> PreFetchItem( int index, CancellationToken cancellationToken )
	{
		if( !Items.TryGetValue( index, out var Item ) )
		{
			await PreFetchNext( index, cancellationToken );

			if( !Items.TryGetValue( index, out Item ) )
				return default;
		}
		return Item.Item;
	}

	private bool InPreFetchNext;


	private async Task PreFetchNext( int index, CancellationToken cancellationToken )
	{
		if( !InPreFetchNext )
		{
			InPreFetchNext = true;

			try
			{
				var Now = DateTime.UtcNow;

				foreach( var Item in await GetNext( index, PreFetchCount, cancellationToken ) )
				{
					if( cancellationToken.IsCancellationRequested )
						break;

					var Temp = ( Now, Item );
					Items[ index++ ] = Temp;
					OnUpdated( Temp );
				}
			}
			finally
			{
				InPreFetchNext = false;
			}
		}
	}
#endregion

#region Abstracts
	protected abstract Task<T[]> GetNext( int currentIndex, int PreFetchCount, CancellationToken cancellationToken );

	internal abstract Task<int> TotalCount();
	internal abstract void      OnUpdated( (DateTime LastUpdatedUTC, T? Item) updated );
#endregion
}