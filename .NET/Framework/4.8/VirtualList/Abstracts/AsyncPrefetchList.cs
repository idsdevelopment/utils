﻿using System.Diagnostics.CodeAnalysis;

namespace Utils.VirtualList.Abstracts;

public abstract class AsyncPrefetchList<T> : IList<T>
{
	public int Count
	{
		get
		{
			return Task.Run( async () =>
			                 {
				                 try
				                 {
					                 return await Fetch.TotalCount();
				                 }
				                 catch
				                 {
					                 return 0;
				                 }
			                 } ).Result;
		}
	}

	public bool IsReadOnly => true;

	private readonly Prefetch Fetch;

	protected internal AsyncPrefetchList( int preFetchCount, CancellationToken cancellationToken = new() )
	{
		Fetch = new Prefetch( this, preFetchCount, cancellationToken );
	}

	[SuppressMessage( "ReSharper", "ParameterHidesPrimaryConstructorParameter" )]
	internal class Prefetch( AsyncPrefetchList<T> owner, int preFetchCount, CancellationToken cancellationToken = new() ) : AsyncPrefetchEnumerable<T>( preFetchCount, cancellationToken )
	{
		protected override Task<T[]> GetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken ) => owner.InternalGetNext( currentIndex, preFetchCount, cancellationToken );

		internal override Task<int> TotalCount() => owner.TotalCount();

		internal override void OnUpdated( (DateTime LastUpdatedUTC, T? Item) updated )
		{
			owner.OnUpdated( updated );
		}
	}

	public T? this[ int index ]
	{
 #pragma warning disable CS8766 // Nullability of reference types in return type doesn't match implicitly implemented member (possibly because of nullability attributes).
		get
		{
			return Task.Run( async () =>
			                 {
				                 try
				                 {
					                 return await Fetch[ index ];
				                 }
				                 catch
				                 {
					                 return default;
				                 }
			                 } ).Result;
		}
		set => throw new NotSupportedException();
#pragma warning restore CS8766 // Nullability of reference types in return type doesn't match implicitly implemented member (possibly because of nullability attributes).
	}

	IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	public IEnumerator<T> GetEnumerator() => Fetch;

	public void Add( T item )
	{
		throw new NotSupportedException();
	}

	public void Clear()
	{
		throw new NotSupportedException();
	}

	public bool Contains( T item ) => throw new NotImplementedException();

	public void CopyTo( T[] array, int arrayIndex )
	{
		throw new NotSupportedException();
	}

	public bool Remove( T item ) => throw new NotSupportedException();
	public int IndexOf( T item ) => throw new NotSupportedException();

	public void Insert( int index, T item )
	{
		throw new NotSupportedException();
	}

	public void RemoveAt( int index )
	{
		throw new NotSupportedException();
	}

#region Abstracts
	protected abstract Task<T[]> InternalGetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken );
	protected abstract Task<int> TotalCount();
	protected abstract void OnUpdated( (DateTime LastUpdatedUTC, T? Item) updated );
#endregion
}