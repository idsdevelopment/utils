﻿using Utils.VirtualList.Abstracts;

namespace Utils.VirtualList;

public abstract class VList<T> : AsyncPrefetchList<string>
{
	protected abstract Task<T[]> GetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken );

	private sealed class BackingAsyncPrefetchList : AsyncPrefetchList<T>
	{
		private readonly VList<T> Owner;

		internal BackingAsyncPrefetchList( VList<T> owner, int preFetchCount, CancellationToken cancellationToken = new CancellationToken() ) : base( preFetchCount, cancellationToken )
		{
			Owner = owner;
		}

		protected override Task<T[]> InternalGetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken )
		{
			return Owner.GetNext( currentIndex, preFetchCount, cancellationToken );
		}

		protected override Task<int> TotalCount() => Owner.TotalCount();

		protected override void OnUpdated( (DateTime LastUpdatedUTC, T? Item) updated )
		{
			Owner.OnUpdated( updated );
		}
	}

	protected void OnUpdated( (DateTime LastUpdatedUTC, T? Item) updated )
	{
	}

	protected sealed override async Task<string[]> InternalGetNext( int currentIndex, int preFetchCount, CancellationToken cancellationToken )
	{
		try
		{
			var Items = await GetNext( currentIndex, preFetchCount, cancellationToken );

			return ( from I in Items
				     select ToString( I ) ).ToArray();
		}
		catch
		{
		}
		return Array.Empty<string>();
	}


	protected internal abstract string ToString( T item );

	// ReSharper disable once PublicConstructorInAbstractClass
	public VList( int preFetchCount, CancellationToken cancellationToken = new() ) : base( preFetchCount, cancellationToken )
	{
		new BackingAsyncPrefetchList( this, preFetchCount, cancellationToken );
	}
}