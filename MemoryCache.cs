﻿using System.Timers;
using Timer = System.Timers.Timer;

// ReSharper disable UseDeconstruction

// ReSharper disable InconsistentlySynchronizedField

namespace Utils;

// Must be outside generic
public abstract class Cleanup
{
	private static readonly List<Action<DateTime>> OnCleanupList = [];
	private static readonly Timer                  Timer         = new( 1_000 ) { AutoReset = true };

	private Action<DateTime>? SaveAction;

	static Cleanup()
	{
		Timer.Elapsed += OnTimerOnElapsed;
		Timer.Start();
	}

	private static void OnTimerOnElapsed( object? o, ElapsedEventArgs elapsedEventArgs )
	{
		lock( OnCleanupList )
		{
			var Now = DateTime.UtcNow;

			foreach( var Action in OnCleanupList )
				Action( Now );
		}
	}

	~Cleanup()
	{
		if( SaveAction is not null )
		{
			lock( OnCleanupList )
			{
				OnCleanupList.Remove( SaveAction );
				SaveAction = null;
			}
		}
	}

	protected void AddAction( Action<DateTime> action )
	{
		SaveAction = action;

		lock( OnCleanupList )
			OnCleanupList.Add( action );
	}
}

public class MemoryCache<TKey, TValue> : Cleanup where TKey : notnull
{
	private readonly Dictionary<TKey, CacheEntry> Dict = new();

#if NET9_0_OR_GREATER
	private readonly Lock LockObject = new();
#else
	private readonly object LockObject = new();
#endif

	public MemoryCache()
	{
		AddAction( now =>
		           {
			           lock( LockObject )
			           {
				           OnBeginCleanup();

				           foreach( var KeyValuePair in ( from D in Dict
				                                          where D.Value.UtcExpires <= now
				                                          select D ).ToList() ) // Must be ToList()
				           {
					           var Key   = KeyValuePair.Key;
					           var Value = KeyValuePair.Value;

					           Dict.Remove( Key );
					           Value.OnExpires?.Invoke( Key, Value.Value );
				           }

				           OnEndCleanup();
			           }
		           } );
	}


	protected virtual TValue? OnNotInCache( TKey key ) => default;

	public void Clear()
	{
		lock( LockObject )
			Dict.Clear();
	}

	public virtual (bool Found, TValue Value) TryGetValue( TKey key )
	{
		lock( LockObject )
		{
			var Ok = Dict.TryGetValue( key, out var Entry );

			if( Ok )
			{
				if( Entry is not null )
				{
					var Expires = Entry.Expires;

					if( Expires is not null )
						Entry.UtcExpires = DateTime.UtcNow.Add( Expires.Value );

					return ( true, Entry.Value );
				}
			}
			else
			{
				var Value = OnNotInCache( key );

				if( Value is not null )
					return ( true, Value );
			}
			return ( false, default! );
		}
	}

	public bool TryGetValue( TKey key, out TValue value )
	{
		var (Ok, Value) = TryGetValue( key );
		value           = Value;
		return Ok;
	}

	public bool ContainsKey( TKey key )
	{
		lock( LockObject )
			return Dict.ContainsKey( key );
	}

	public void AddSliding( TKey key, TValue value, TimeSpan expires, Action<TKey, TValue>? onExpires )
	{
		lock( LockObject )
		{
			Dict[ key ] = new CacheEntry
			              {
				              Value      = value,
				              OnExpires  = onExpires,
				              Expires    = expires,
				              UtcExpires = DateTime.UtcNow.Add( expires )
			              };
		}
	}

	public void AddSliding( TKey key, TValue value, TimeSpan expires )
	{
		AddSliding( key, value, expires, null );
	}

	public void AddAbsolute( TKey key, TValue value, DateTimeOffset expires, Action<TKey, TValue>? onExpires )
	{
		lock( LockObject )
		{
			Dict[ key ] = new CacheEntry
			              {
				              Value      = value,
				              OnExpires  = onExpires,
				              UtcExpires = expires.UtcDateTime
			              };
		}
	}

	public void AddAbsolute( TKey key, TValue value, TimeSpan expires, Action<TKey, TValue>? onExpires )
	{
		AddAbsolute( key, value, DateTime.UtcNow.Add( expires ), onExpires );
	}


	public void AddAbsolute( TKey key, TValue value, DateTimeOffset expires )
	{
		AddAbsolute( key, value, expires, null );
	}


	public void AddAbsolute( TKey key, TValue value, TimeSpan expires )
	{
		AddAbsolute( key, value, expires, null );
	}

	public bool Remove( TKey key )
	{
		lock( LockObject )
			return Dict.Remove( key );
	}

	private class CacheEntry
	{
		internal TimeSpan?             Expires;
		internal Action<TKey, TValue>? OnExpires;
		internal DateTime              UtcExpires = DateTime.MaxValue;
		internal TValue                Value      = default!;
	}

	public (bool Ok, TValue Value) this[ TKey key ] => TryGetValue( key );

#region Cleanup
	protected virtual void OnBeginCleanup()
	{
	}

	protected virtual void OnEndCleanup()
	{
	}
#endregion
}