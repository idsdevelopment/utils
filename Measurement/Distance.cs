﻿// ReSharper disable IdentifierTypo

// ReSharper disable InconsistentNaming

namespace Utils;

public struct Measurement
{
	public decimal Metres { get; set; }

	public decimal KiloMetres
	{
		get => Metres / 1000;
		set => Metres = value * 1000;
	}

	public decimal CentiMetres
	{
		get => Metres * 100;
		set => Metres = value / 100;
	}

	public decimal MilliiMetres
	{
		get => Metres * 1000;
		set => Metres = value / 1000;
	}

	public decimal Miles
	{
		get => Metres / 1609.344M;
		set => Metres = value * 1609.344M;
	}


	public decimal Yard
	{
		get => Metres / 0.9144M;
		set => Metres = value * 0.9144M;
	}

	public decimal Feet
	{
		get => Metres / 0.3048M;
		set => Metres = value * 0.3048M;
	}

	public static implicit operator Measurement(decimal value) => new()
	{
		Metres = value
	};

	public static implicit operator Measurement(double value) => (decimal)value;

	#region Simple Distance
	public static Measurement StraightLineDistance(decimal p1aMetres, decimal p1bMetres, decimal p2aMetres, decimal p2bMetres) => StraightLineDistance((double)p1aMetres, (double)p1bMetres, (double)p2aMetres, (double)p2bMetres);

	public static Measurement StraightLineDistance(double p1aMetres, double p1bMetres, double p2aMetres, double p2bMetres)
	{
		var D1 = p2aMetres - p1aMetres;
		var D2 = p2bMetres - p1bMetres;
		return Math.Abs(Math.Sqrt((D1 * D1) + (D2 * D2)));
	}


	public static Measurement SimpleGpsDistance(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude)
	{
		var Rlat1 = (Math.PI * fromLatitude) / 180;
		var Rlat2 = (Math.PI * toLatitude) / 180;
		var Theta = fromLongitude - toLongitude;
		var Rtheta = (Math.PI * Theta) / 180;

		var Dist = (Math.Sin(Rlat1) * Math.Sin(Rlat2)) + (Math.Cos(Rlat1) * Math.Cos(Rlat2) * Math.Cos(Rtheta));

		Dist = Math.Acos(Dist);
		Dist = (Dist * 180) / Math.PI;
		Dist = Dist * 60 * 1.1515;

		if (double.IsNaN(Dist))
			Dist = 0;

		return new Measurement
		{
			Miles = (decimal)Dist
		};
	}

	public static Measurement SimpleGpsDistance(decimal fromLatitude, decimal fromLongitude, decimal toLatitude, decimal toLongitude) => SimpleGpsDistance((double)fromLatitude, (double)fromLongitude, (double)toLatitude, (double)toLongitude);
	#endregion

	#region Vincenty Distance
	internal struct Ellipsoid
	{
		internal double A,
						B,
						F;
	}

	public enum ELLIPSOID_MODEL
	{
		WGS_84,
		GRS_80,
		AIRY_1830,
		INTL_1924,
		CLARKE_1880,
		GRS_67
	}

	private static readonly Ellipsoid[] Ellipsoids =
	{
		new() { A = 6378137, B     = 6356752.3142, F  = 1 / 298.257223563 }, // WGS-84
		new() { A = 6378137, B     = 6356752.3141, F  = 1 / 298.257222101 }, // GRS-80
		new() { A = 6377563.396, B = 6356256.909, F   = 1 / 299.3249646 },   // Airy (1830)
		new() { A = 6378388, B     = 6356911.946, F   = 1 / 297.0 },         // Intl (1929)
		new() { A = 6378249.145, B = 6356514.86955, F = 1 / 293.465 },       // Clark (1880)
		new() { A = 6378160, B     = 6356774.719, F   = 1 / 298.25 }         // GRS-67
	};

	public static decimal DegreesToRadians(decimal angle) => (decimal)((Math.PI / 180) * (double)angle);
	public static double DegreesToRadians(double angle) => (Math.PI / 180) * angle;

	public Measurement DistanceVincenty(double fromLatitude, double fromLongitude, double toLatitude, double toLongitude, ELLIPSOID_MODEL model = ELLIPSOID_MODEL.WGS_84)
	{
		var Ellipsoid = Ellipsoids[(int)model];

		double SinSigma = 0,
			   Cos2SigmaM = 0,
			   CosSigma = 0,
			   CosSqAlpha = 0,
			   Sigma = 0;

		var L = DegreesToRadians(toLongitude - fromLongitude);

		var U1 = Math.Atan((1 - Ellipsoid.F) * Math.Tan(DegreesToRadians(fromLatitude)));
		var SinU1 = Math.Sin(U1);
		var CosU1 = Math.Cos(U1);

		var U2 = Math.Atan((1 - Ellipsoid.F) * Math.Tan(DegreesToRadians(toLatitude)));
		var SinU2 = Math.Sin(U2);
		var CosU2 = Math.Cos(U2);

		var Lambda = L;
		var LambdaP = 2 * Math.PI;

		var Limit = 20;

		for (; Limit > 0; Limit--)
		{
			if (Math.Abs(Lambda - LambdaP) <= 1e-12)
				break;

			var SinLambda = Math.Sin(Lambda);
			var CosLambda = Math.Cos(Lambda);

			var CosU2SinLambda = CosU2 * SinLambda;
			var CosU1SinU2SinU1CosU2CosLambda = (CosU1 * SinU2) - (SinU1 * CosU2 * CosLambda);

			SinSigma = Math.Sqrt((CosU2SinLambda * CosU2SinLambda) + (CosU1SinU2SinU1CosU2CosLambda * CosU1SinU2SinU1CosU2CosLambda));

			if (Math.Abs(SinSigma) < 1e-12)
				return new Measurement(); // co-incident points

			CosSigma = (SinU1 * SinU2) + (CosU1 * CosU2 * CosLambda);
			Sigma = Math.Atan2(SinSigma, CosSigma);

			var SinAlpha = (CosU1 * CosU2 * SinLambda) / SinSigma;
			CosSqAlpha = 1 - (SinAlpha * SinAlpha);

			try
			{
				Cos2SigmaM = CosSigma - ((2 * SinU1 * SinU2) / CosSqAlpha);
			}
			catch
			{
				Cos2SigmaM = 0; // equatorial line
			}

			var C = (Ellipsoid.F / 16) * CosSqAlpha * (4 + (Ellipsoid.F * (4 - (3 * CosSqAlpha))));

			LambdaP = Lambda;

			Lambda = L + ((1 - C) * Ellipsoid.F * SinAlpha * (Sigma + (C * SinSigma * (Cos2SigmaM + (C * CosSigma * (-1 + (2 * Cos2SigmaM * Cos2SigmaM)))))));
		}

		var Result = new Measurement();

		if (Limit != 0)
		{
			var EaSq = Ellipsoid.A * Ellipsoid.A;
			var EbSq = Ellipsoid.B * Ellipsoid.B;

			var Usq = (CosSqAlpha * (EaSq - EbSq)) / EbSq;

			var A1 = 1 + ((Usq / 16384) * (4096 + (Usq * (-768 + (Usq * (320 - (175 * Usq)))))));
			var B1 = (Usq / 1024) * (256 + (Usq * (-128 + (Usq * (74 - (47 * Usq))))));

			var DeltaSigma = B1 * SinSigma * (Cos2SigmaM + ((B1 / 4) * ((CosSigma * (-1 + (2 * Cos2SigmaM * Cos2SigmaM)))
																			 - ((B1 / 6) * Cos2SigmaM * (-3 + (4 * SinSigma * SinSigma)) * (-3 + (4 * Cos2SigmaM * Cos2SigmaM))))));

			Result.Metres = (decimal)Math.Abs(Ellipsoid.B * A1 * (Sigma - DeltaSigma));
		}
		return Result;
	}
	#endregion
}