﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Utils;

public class ObservableDictionary<TKey, TValue> : SortedDictionary<TKey, TValue>,
                                                  INotifyCollectionChanged,
                                                  INotifyPropertyChanged where TKey : notnull
{
	public readonly  ObservableCollection<TValue> Collection = [];
	private readonly object                       LockObject = new();

	public new void Add( TKey key, TValue value )
	{
		if( key is null )
			throw new ArgumentNullException( nameof( key ) );

		lock( LockObject )
		{
			base.Add( key, value );

			var Ndx = Collection.IndexOf( value );

			if( Ndx > -1 )
				Collection[ Ndx ] = value;
			else
				Collection.Add( value );
		}

		OnPropertyChanged( nameof( Collection ) );
		OnPropertyChanged();
		OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>( key, value ) ) );
	}

	public new bool Remove( TKey key )
	{
		if( key is null )
			throw new ArgumentNullException( nameof( key ) );

		bool Result;

		lock( LockObject )
		{
			if( TryGetValue( key, out var Value ) )
				Collection.Remove( Value );

			Result = base.Remove( key );
		}

		if( Result )
		{
			OnPropertyChanged( nameof( Collection ) );
			OnPropertyChanged();
			OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Remove, key ) );
		}
		return Result;
	}

	public new void Clear()
	{
		lock( LockObject )
		{
			Collection.Clear();
			base.Clear();
		}
		OnPropertyChanged( nameof( Collection ) );
		OnPropertyChanged();
		OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
	}

	public new TValue this[ TKey key ]
	{
		get
		{
			if( key is null )
				throw new ArgumentNullException( nameof( key ) );

			lock( LockObject )
				return base[ key ];
		}
		set
		{
			if( key is null )
				throw new ArgumentNullException( nameof( key ) );

			TValue OldValue;

			lock( LockObject )
			{
				OldValue    = base[ key ];
				base[ key ] = value;

				var Ndx = Collection.IndexOf( OldValue );

				if( Ndx > -1 )
					Collection[ Ndx ] = value;
			}

			OnPropertyChanged( nameof( Collection ) );
			OnPropertyChanged();
			OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Replace, new KeyValuePair<TKey, TValue>( key, value ), new KeyValuePair<TKey, TValue>( key, OldValue ) ) );
		}
	}

#region Events
	public event NotifyCollectionChangedEventHandler? CollectionChanged;
	public event PropertyChangedEventHandler?         PropertyChanged;

	protected virtual void OnPropertyChanged( [CallerMemberName] string? propertyName = null )
	{
		PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
	}

	protected virtual void OnCollectionChanged( NotifyCollectionChangedEventArgs arg )
	{
		CollectionChanged?.Invoke( this, arg );
	}
#endregion
}