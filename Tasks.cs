﻿#nullable enable

using System.Net;
using System.Timers;

using Timer = System.Timers.Timer;

// ReSharper disable InconsistentNaming
// ReSharper disable AccessToModifiedClosure
// ReSharper disable CollectionNeverQueried.Local

namespace Utils;

public static class RandomDelay
{
	public static void Delay(int minSeconds, int maxSeconds)
	{
		var Delay = new Random().Next(minSeconds * 1000, maxSeconds * 1000);
		Thread.Sleep(Delay);
	}

	public static void Delay()
	{
		Delay(0, 59);
	}

	public static Task DelayAsync(int minSeconds, int maxSeconds)
	{
		var Delay = new Random().Next(minSeconds * 1000, maxSeconds * 1000);
		return Task.Delay(Delay);
	}

	public static Task DelayAsync() => DelayAsync(0, 59);
}

public class Tasks
{
	public class Task<T>
	{
		public System.Threading.Tasks.Task<IList<T>> RunAsync(IList<T> itemList, Func<T, Task> task, int maxTasks)
		{
			var Count = itemList.Count;

			if (Count > 0)
			{
				if (maxTasks < 1)
					maxTasks = 1;

				var TaskCount = Math.Min(maxTasks, Count);
				var Tasks = new Task[TaskCount];

				var TaskNdx = 0;
				var Ndx = 0;

				for (; (Count > 0) && (TaskNdx < TaskCount); --Count)
				{
					var Item = itemList[Ndx++];

					Tasks[TaskNdx++] = Task.Run(async () =>
												   {
													   await task(Item);
												   });
				}

				for (; Count > 0; Count--)
				{
					var TskNdx = Task.WaitAny(Tasks);

					if (TskNdx >= 0)
					{
						var Item = itemList[Ndx++];

						Tasks[TskNdx] = Task.Run(async () =>
													{
														await task(Item);
													});
					}
					else
						throw new Exception("Lost Task");
				}

				Task.WaitAll(Tasks);
			}

			return Task.FromResult(itemList);
		}

		public IList<T> Run(IList<T> itemList, Action<T> task, int maxTasks)
		{
			return RunAsync(itemList, async item =>
									   {
										   await Task.Run(() =>
														   {
															   task(item);
														   });
									   }, maxTasks).ConfigureAwait(false).GetAwaiter().GetResult();
		}

		public async System.Threading.Tasks.Task<IList<T>> RunAsync(IList<T> itemList, Func<T, Task> task) => await RunAsync(itemList, task, MaxThreads);

		public IList<T> Run(IList<T> itemList, Action<T> task)
		{
			return RunAsync(itemList, async item =>
									   {
										   await Task.Run(() =>
														   {
															   task(item);
														   });
									   }).ConfigureAwait(false).GetAwaiter().GetResult();
		}
	}

	public class ConnectionTask<T> : Task<T>
	{
#pragma warning disable SYSLIB0014
		public new IList<T> Run(IList<T> itemList, Action<T> action) => Run(itemList, action, (ServicePointManager.DefaultConnectionLimit * 3) / 4); // Use up 3/4 of the connections
#pragma warning restore SYSLIB0014
	}

	public static int MaxThreads
	{
		get
		{
			if (_MaxThreads <= 0)
				_MaxThreads = Environment.ProcessorCount;

			return _MaxThreads;
		}
	}


	private static int _MaxThreads;

	#region WaitUntil
	public static (bool Ok, T Value) WaitUntil<T>(int maxWaitTimeInMs, int pollingIntervalInMs, Func<(bool, T)> pollingTest)
	{
		var Start = DateTime.UtcNow;

		while (true)
		{
			var Result = pollingTest();

			if (Result.Item1)
				return Result;

			Thread.Sleep(pollingIntervalInMs);

			if ((DateTime.UtcNow - Start).TotalMilliseconds >= maxWaitTimeInMs)
				return (false, default)!;
		}
	}

	public static async System.Threading.Tasks.Task<(bool Ok, T Value)> WaitUntil<T>(int maxWaitTimeInMs, int pollingIntervalInMs, Func<System.Threading.Tasks.Task<(bool, T)>> pollingTest)
	{
		var Start = DateTime.UtcNow;

		while (true)
		{
			var Result = await pollingTest();

			if (Result.Item1)
				return Result;

			Thread.Sleep(pollingIntervalInMs);

			if ((DateTime.UtcNow - Start).TotalMilliseconds >= maxWaitTimeInMs)
				return (false, default)!;
		}
	}
	#endregion

	#region RunVoid
	private static readonly HashSet<Task> _Tasks = [];
	private static readonly HashSet<Timer> _Timers = [];

	public static Task RunVoid(Action action)
	{
		lock (_Tasks)
		{
			Task CurrentTask = null!;

			CurrentTask = Task.Run(() =>
									{
										try
										{
											action();
										}
										catch
										{
										}
										finally
										{
											lock (_Tasks)
												_Tasks.Remove(CurrentTask);
										}
									});

			_Tasks.Add(CurrentTask);
			return CurrentTask;
		}
	}


	public static void RunVoid(int delay, Action action)
	{
		lock (_Timers)
		{
			var T = new Timer(delay)
			{
				AutoReset = false
			};

			_Timers.Add(T);

			void OnElapsed(object? sender, ElapsedEventArgs elapsedEventArgs)
			{
				var CanRun = true; // Timer can still run even when timer is disposed

				if (sender is Timer Timer)
				{
					lock (_Timers)
						CanRun = _Timers.Remove(Timer);

					if (CanRun)
					{
						Timer.Elapsed -= OnElapsed;
						Timer.Dispose();
					}
				}

				if (CanRun)
					RunVoid(action);
			}

			T.Elapsed += OnElapsed;
			T.Start();
		}
	}
	#endregion
}