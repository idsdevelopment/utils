﻿#nullable enable

// ReSharper disable ReplaceSubstringWithRangeIndexer

namespace Utils;

public static class Strings
{
	public static string ToHex(this string s, string separator = " ", bool zeroSuppress = true)
	{
		var Builder = new StringBuilder();

		static char ToChar(byte nibble)
		{
			nibble &= 0xf;

			if (nibble <= 9)
				return (char)((byte)'0' + nibble);

			return (char)(((byte)'A' + nibble) - 10);
		}

		static string ByteToStr(byte b)
		{
			return ToChar((byte)(b >> 4)).ToString() + ToChar(b);
		}

		static string ToStr(ushort word)
		{
			return ByteToStr((byte)(word >> 8)) + ByteToStr((byte)word);
		}

		var First = true;

		foreach (var C in s)
		{
			if (!First)
				Builder.Append(separator);
			else
				First = false;

			var Hex = ToStr(C);

			if (zeroSuppress)
			{
				Hex = Hex.TrimStart('0');

				switch (Hex.Length)
				{
					case 0:
						Hex = "00";
						break;

					case 1:
					case 3:
						Hex = '0' + Hex;
						break;
				}
			}

			Builder.Append(Hex);
		}

		return Builder.ToString();
	}

	public static string AlphaNumericAndSpaces(this string str, bool trim = false, bool pack = false)
	{
		var Result = new StringBuilder();

		foreach (var C in str)
		{
			switch (C)
			{
				case (char)0x8 or ' ':
					Result.Append(' ');
					break;

				case (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or (>= '0' and <= '9'):
					Result.Append(C);
					break;
			}
		}

		var Str = Result.ToString();

		return pack ? Str.Pack(trim) :
			   trim ? Str.Trim() : Str;
	}

	public static string ReplaceLastOccurrence(this string source, string find, string replace, StringComparison comparison = StringComparison.Ordinal)
	{
		var Place = source.LastIndexOf(find, comparison);

		return Place != -1 ? source.Remove(Place, find.Length).Insert(Place, replace) : source;
	}

	public static string TrimStart(this string target, string trimString, StringComparison comparison)
	{
		if (!string.IsNullOrEmpty(target) && !string.IsNullOrEmpty(trimString))
		{
			var Length = trimString.Length;

			while (target.StartsWith(trimString, comparison))
				target = target.Substring(Length);
		}
		return target;
	}

	public static string TrimToLower(this string txt) => txt.Trim().ToLower();
	public static string TrimToUpper(this string txt) => txt.Trim().ToUpper();

	public static bool Contains(this string text, string value, StringComparison stringComparison) => text.IndexOf(value, stringComparison) >= 0;

	public static string NullTrim(this string? str) => str is null ? "" : str.Trim();

	public static string SubStr(this string str, int startIndex, int length)
	{
		try
		{
			return (startIndex + length) > str.Length ? str.Substring(startIndex) : str.Substring(startIndex, length);
		}
		catch
		{
			return "";
		}
	}

	public static string ToNumeric(this string txt, bool allowDecimal = false)
	{
		var HasDecimal = !allowDecimal;

		if (HasDecimal) // Remove anything from the decimal point onwards
		{
			var Pos = txt.IndexOf('.');

			if (Pos >= 0)
				txt = txt.Substring(0, Pos);
		}

		var HasSign = false;
		var HasDigit = false;

		var Result = new StringBuilder();

		foreach (var C in txt)
		{
			switch (C)
			{
				case '+':
				case '-':
					if (!HasSign)
					{
						if (C != '+')
							Result.Append(C);
					}

					break;

				case '.':
					if (!HasDecimal)
					{
						HasDecimal = true;
						Result.Append(HasDigit ? "." : "0.");
					}

					break;

				default:
					if (C is >= '0' and <= '9')
					{
						HasDigit = true;
						Result.Append(C);
						break;
					}
					else
						continue;
			}

			HasSign = true;
		}

		return Result.ToString().TrimEnd('.', '-');
	}


	public static bool ToBool(this string value)
	{
		return value.Trim().ToLower() switch
		{
			"true" => true,
			"t" => true,
			_ => false
		};
	}

	public static int Compare(this string str1, string str2, StringComparison strCmp = StringComparison.InvariantCulture) => string.Compare(str1, str2, strCmp);

	public static bool IsNullOrEmpty(this string? str) => string.IsNullOrEmpty(str);

	public static bool IsNotNullOrEmpty(this string? str) => !string.IsNullOrEmpty(str);

	public static bool IsNullOrWhiteSpace(this string? str) => string.IsNullOrWhiteSpace(str);

	public static bool IsNotNullOrWhiteSpace(this string? str) => !string.IsNullOrWhiteSpace(str);


	public static bool IsAlpha(this char c) => c is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z');

	public static bool IsAlphaNumeric(this char c) => c is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or (>= '0' and <= '9');

	public static bool IsAlphaNumeric(this string txt)
	{
		foreach (var C in txt)
		{
			if (!C.IsAlphaNumeric())
				return false;
		}
		return true;
	}

	public static string Capitalise(this string value) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower());

	public static string ToAlphaNumeric(this string txt)
	{
		var Result = new StringBuilder();

		if (txt.IsNotNullOrWhiteSpace())
		{
			foreach (var C in txt)
			{
				if (C.IsAlphaNumeric())
					Result.Append(C);
			}
		}

		return Result.ToString();
	}

	public static string ToIdent(this string value)
	{
		var First = true;
		var Retval = new StringBuilder();

		foreach (var C in value)
		{
			if (C is (>= 'A' and <= 'Z') or (>= 'a' and <= 'z') or '_')
				Retval.Append(C);

			else if (!First && C is >= '0' and <= '9')
				Retval.Append(C);
			else
				Retval.Append('_');

			First = false;
		}

		return Retval.ToString();
	}

	public static bool IsInteger(this string value) => long.TryParse(value, out _);
	public static bool IsNumeric(this string value) => double.TryParse(value, out _);


	public static string? Between(this string text, string first, string last, StringComparison culture = StringComparison.Ordinal)
	{
		var First = text.IndexOf(first, culture);

		if (First >= 0)
		{
			First += first.Length;

			var Last = text.LastIndexOf(last, culture);

			if ((Last >= 0) && (Last > First))
				return text.Substring(First, Last - First);
		}

		return null;
	}

	public static string Pack(this string str, bool trim = true)
	{
		if (trim)
			str = str.Trim();

		var Result = new StringBuilder();

		var WasSpace = false;

		foreach (var C in str)
		{
			if (C <= ' ')
			{
				if (!WasSpace)
				{
					WasSpace = true;
					Result.Append(' ');
				}
			}
			else
			{
				WasSpace = false;
				Result.Append(C);
			}
		}
		return Result.ToString();
	}

	public static bool IsIdent(this string id)
	{
		var First = true;

		foreach (var C in id)
		{
			if (C is (>= 'a') and (<= 'z') or (>= 'A' and <= 'Z') || (!First && C is (>= '0' and <= '9')))
				First = false;
			else
				return false;
		}

		return true;
	}

	public static string MaxLength(this string? value, int maxLength) => value is null ? "" : value.Substring(0, Math.Min(value.Length, maxLength));
}