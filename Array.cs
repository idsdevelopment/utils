namespace Utils;

public static class ArrayExtensions
{
	/// <summary>
	///     Copy Array ( Same as new C# .. )
	///     to is Exclusive
	/// </summary>
	/// <param
	///     name="array">
	/// </param>
	/// <param
	///     name="from">
	/// </param>
	/// <param
	///     name="to">
	/// </param>
	/// <returns>object[]</returns>
	public static object[] Range( this object[] array, int from, int to )
	{
		var Len  = to - from;
		var Temp = new object[ Len ];
		Array.Copy( array, from, Temp, 0, Len );

		return Temp;
	}

	// byte[] is implicitly convertible to ReadOnlySpan<byte>
	public static bool ArrayCompare( ReadOnlySpan<byte> a1, ReadOnlySpan<byte> a2 ) => a1.SequenceEqual( a2 );

	public static bool ArrayCompare( ReadOnlySpan<int> a1, ReadOnlySpan<int> a2 ) => a1.SequenceEqual( a2 );
}